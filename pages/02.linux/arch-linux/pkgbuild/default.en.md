---
title: PKGBUILD
visible: true
---

[toc]

_TODO:_ patching  
https://wiki.archlinux.org/index.php?title=Patching_packages&useskinversion=1

Get hashsums:

[shuser]

```
makepkg -g
```

[/shuser]

Build package:

[shuser]

```
makepkg
```

[/shuser]

Build package with AUR dependencies:  
_Officially, AUR-helpers are not recommended and AUR-dependencies should be downloaded manually_

[shuser]

```
paru -U
```

[/shuser]

Afterwards install with `paru -U <package>.tar.zst`

Install dependencies:

[shuser]

```
makepkg -s
```

[/shuser]

Install package after successful build:

[shuser]

```
makepkg -i
```

[/shuser]

Just extract package and run `prepare()`  
`--nobuild`

Don't extract package and don't run `prepare()`  
`--noextract`

Create SRCINFO

[shuser]

```
makepkg --printsrcinfo > .SRCINFO
```

[/shuser]

Using paru to build package in clean chroot and install

[shuser]

```
paru --chroot -Ui
```

[/shuser]

## Upload new package

Initialise new repo with `master` branch

[shuser]

```
git -c init.defaultbranch=master init
```

[/shuser]

Add remote for AUR and fetch to initialise

[shuser]

```
git remote add aur ssh://aur@aur.archlinux.org/[PKGBASE].git
git fetch aur
```

[/shuser]
