---
title: "DHCP Server and Routing"
visible: true
---

[toc]

## Installation

[shroot]

```sh
apt install isc-dhcp-server
```

[/shroot]

## Configuration

Edit `/etc/default/isc-dhcp-server`

```
INTERFACESv4="[INTERFACE 1] [INTERFACE 2]"
```

Edit `/etc/dhcp/dhcpd.conf` to set a subnet

```
subnet [NETADDRESS] netmask [SUBNETMASK] {
    range [FIRST DHCP] [LAST DHCP];
    option subnet-mask [SUBNETMASK];
    option routers [GATEWAY];
    option domain-name "[NAME]";
    option domain-name-servers [DNS SERVER];
}
```

Edit `/etc/network/interfaces`

```
auto [INTERFACE]
iface [INTERFACE] inet static
    address [ADDRESS]
    network [NETADDRESS]
    netmask [NETMASK]
    broadcast [BROADCAST]
```

Enable the interface

[shroot]

```sh
ifup [INTERFACE]
```

[/shroot]

Restart DHCP Server

[shroot]

```sh
systemctl restart isc-dhcp-server.service
```

[/shroot]

### Enable routing

[shroot]

```sh
echo "net.ipv4.ip_forward=1" >> /etc/sysctl.d/80-forwarding.conf
sysctl -p /etc/sysctl.d/80-forwarding.conf
```

[/shroot]

[shroot]

```sh
iptables -t nat -A POSTROUTING -o [WAN INTERFACE] -j MASQUERADE
iptables -A FORWARD -i [LAN INTERFACE] -j ACCEPT
```

[/shroot]

Make iptables permanent  
Select `Yes` during the installation to save current rules

[shroot]

```sh
apt install iptables-persistent
```

[/shroot]

### Enable DHCP-managed fixed IP address

```
host [HOSTNAME] {
  hardware ethernet [MAC ADDRESS];
  fixed-address [IP ADDRESS];
}
```

### Dynamic DNS

_Needs a supported DNS like BIND or PowerDNS_  
[Configure your DNS server to accept updates](/linux/services/powerdns)  
Add the following snippet to your `/etc/dhcp/dhcpd.conf` file  
How to generate the key is also described in the DNS article

```
ddns-updates on;
ddns-update-style interim;
update-static-leases on;

ddns-domainname "testpdns";
ddns-rev-domainname "in-addr.arpa.";

key "[KEYNAME]" {
    algorithm hmac-md5;
    secret "[KEY]";
};

zone testpdns {
    primary 127.0.0.1;
    key [KEYNAME];
}

zone 7.168.192.in-addr.arpa. {
    primary 127.0.0.1;
    key [KEYNAME];
}
```

## Client

DHCP Request

[shroot]

```sh
dhclient -v
```

[/shroot]

Release IP

[shroot]

```sh
dhclient -v -r
```

[/shroot]
