---
title: PostgreSQL
visible: true
---

[toc]
## Installation
### Debian
Install generic main version  
`# apt install postgresql postgresql-client`  
Install specific major version. e.g Version 13  
`# apt install postgresql-13 postgresql-client-13`  

