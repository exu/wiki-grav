---
title: Fstab
visible: true
---

[toc]

## Other drives

Find uuid with `sudo blkid`  
`UUID=[UUID] [MOUNTPATH] [FILESYSTEM] defaults,noatime 0 2`

## Samba shares

```
//[IP]/[PATH]/ [MOUNTPATH] cifs uid=0,credentials=[CREDENTIALS FILE],iocharset=utf8,noperm,nofail 0 0
```

Example credentials file:

```
user=[USER]
password=[PASSWORD]
domain=WORKGROUP
```

Make sure to set permissions to the credential files to something like 700.
