---
title: "Minecraft Server"
visible: true
---

[toc]

Make sure to allow port `25565/tcp` to your server

## Vanilla

## PaperMC

## Forge Server

Download the forge installer from their website and install the server to a directory of your choosing.  
Copy that folder to your server

On the server, create a directory for the server.  
`# mkdir /etc/minecraft`  
Put your folder here

Install java  
`# apt install openjdk-17-jre`

Add a `minecraft` user.

```sh
useradd minecraft
chown minecraft:minecraft -R /etc/minecraft/
```

Start the server a first time.  
`sudo -u minecraft /etc/minecraft/forge-(version)/run.sh`

Accept the EULA by editing `/etc/minecraft/forge-(version)/eula.txt`

## Fabric Server

## Systemd Service

> Adapted from [this gist](https://gist.github.com/dotStart/ea0455714a0942474635)

`/etc/systemd/system/minecraft.service`

```systemd
[Unit]
Description=Minecraft Server
After=network.target

[Service]
User=minecraft
WorkingDirectory=/etc/minecraft/server
# You can customize the maximum amount of memory as well as the JVM flags here
#ExecStart=/usr/bin/java -XX:+UseG1GC -Xmx3G -jar server.jar --nojline --noconsole
ExecStart=/usr/bin/java @user_jvm_args.txt @libraries/net/minecraftforge/forge/1.18.1-39.0.5/unix_args.txt nogui "$@"

# Restart the server when it is stopped or crashed after 30 seconds
# Comment out RestartSec if you want to restart immediately
Restart=always
RestartSec=30

# Alternative: Restart the server only when it stops regularly
# Restart=on-success

# Do not remove this!
StandardInput=null

[Install]
WantedBy=multi-user.target
```

## Systemd Service with STDIN available

> Based on [this stackexchange answer](https://unix.stackexchange.com/questions/598221/how-to-control-systemd-service-using-screen/612118#612118)

`/etc/systemd/system/minecraft.socket`

```systemd
[Unit]
PartOf=minecraft.service

[Socket]
ListenFIFO=%t/minecraft.stdin
```

`/etc/systemd/system/minecraft.service`

```systemd
[Unit]
Description=Minecraft Server
After=network.target

[Service]
Type=simple
User=minecraft
WorkingDirectory=/etc/minecraft/server
# fabric
ExecStart=/usr/bin/java -XX:+UseG1GC -Xmx4G -jar /etc/minecraft/server/fabric-server-launch.jar
# forge
#ExecStart=/usr/bin/java @user_jvm_args.txt @libraries/net/minecraftforge/forge/1.18.1-39.0.5/unix_args.txt nogui "$@"
Restart=on-failure
# Socket used for STDIN
Sockets=minecraft.socket
StandardInput=socket
StandardOutput=journal
StandardError=journal

[Install]
WantedBy=multi-user.target
```

To run commands, redirect commands into your socket.  
`echo "command" > /run/minecraft.stdin`

### Pipe commands script

**No safety at all!!**

```sh
#!/usr/bin/env bash
echo "$@" > /run/minecraft.stdin
```
