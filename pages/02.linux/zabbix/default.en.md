---
title: Zabbix
visible: true
---

[toc]

## Zabbix Server

### Pod

```sh
podman pod create --name zabbix -p 127.0.0.1:8080:8080 -p 10051:10051
```

### Database

```sh
podman run --name zabbix-mysql -t \
    -e MYSQL_DATABASE="zabbix" \
    -e MYSQL_USER="zabbix" \
    -e MYSQL_PASSWORD="zabbix" \
    -e MYSQL_ROOT_PASSWORD="rootpass" \
    -v /mnt/mysql/:/var/lib/mysql/:Z \
    --restart=always \
    --pod=zabbix \
    -d docker.io/mysql:latest \
    --character-set-server=utf8 --collation-server=utf8_bin \
    --default-authentication-plugin=mysql_native_password
```

### Application

Zabbix consists of multiple containers that need to be running.  
First is the server itself.

```sh
podman run --name zabbix-server -t \
    -e DB_SERVER_HOST="127.0.0.1" \
    -e MYSQL_DATABASE="zabbix" \
    -e MYSQL_USER="zabbix" \
    -e MYSQL_PASSWORD="zabbix" \
    -e MYSQL_ROOT_PASSWORD="rootpass" \
    -e ZBX_STARTPOLLERS=50 \
    --pod=zabbix \
    -d docker.io/zabbix/zabbix-server-mysql:latest
```

Next, we need the webserver

```sh
podman run --name zabbix-web -t \
    -e ZBX_SERVER_HOST="127.0.0.1" \
    -e DB_SERVER_HOST="127.0.0.1" \
    -e MYSQL_DATABASE="zabbix" \
    -e MYSQL_USER="zabbix" \
    -e MYSQL_PASSWORD="zabbix" \
    -e MYSQL_ROOT_PASSWORD="rootpass" \
    --restart=always \
    --pod=zabbix \
    -d docker.io/zabbix/zabbix-web-nginx-mysql:latest
```

Finally, we will also install the agent as a container

```sh
podman run --name zabbix-agent \
    -e ZBX_SERVER_HOST="127.0.0.1,localhost" \
    --restart=always \
    --pod=zabbix \
    -d docker.io/zabbix/zabbix-agent:latest
```

The default user is `Admin` with password `zabbix`

### Updating Server

Updating the server might fail for various reasons. Those I already encountered will be documented below.  
_NOTE: The server and proxy need to run the same version of zabbix to talk with one another_

#### MARIADB: Missing permissions (log_bin_trust_function_creators)

From what I could find this error is thrown, when the specified user lacks super user privileges.  
A workaround is enabling `log_bin_trust_function_creators` temporarily.  
`# podman exec -it zabbix-mysql bash`  
`# mysql -u root -p` and enter the root password  
`mysql> set global log_bin_trust_function_creators=1;`

The setting will be reset to default after a restart of the database container.

## Zabbix Proxy

`ZBX_HOSTNAME` has to be the same as the value configured on the zabbix server as the proxy name.

```sh
podman run --name zabbix-proxy \
    -p 10051:10051 \
    -e ZBX_SERVER_HOST="178.18.243.82" \
    -e ZBX_HOSTNAME="he1prx1" \
    -e ZBX_TLSCONNECT=psk \
    -e ZBX_TLSPSKIDENTITY="PSK Zabbix he1prx1" \
    -e ZBX_TLSPSKFILE="/mnt/zabbix/zabbix_agentd.psk" \
    -v /mnt/zabbix:/var/lib/zabbix/db_data \
    -v /mnt/zabbix:/mnt/zabbix \
    -d docker.io/zabbix/zabbix-proxy-sqlite3:latest
```

### Updating Proxy

Updating the proxy will always fail when using the SQLite database, as upgrading is not supported for SQLite.  
_NOTE: The server and proxy need to run the same version of zabbix to talk with one another_

Simply deleting/moving the old SQLite database and restarting the proxy is enough.  
_NOTE: History stored on the proxy will obviously be lost_

## Zabbix Agent

```sh
podman run --name zabbix-agent \
    -p 10050:10050 \
    -e ZBX_HOSTNAME="(hostname)" \
    -e ZBX_SERVER_HOST="(zabbix server/proxy)" \
    -d docker.io/zabbix/zabbix-agent:latest
```

### XCP-ng

Use zabbix package from EPEL.  
Zabbix server can handle the older agent fine [See the Documentation on Compatibility](https://www.zabbix.com/documentation/current/en/manual/appendix/compatibility)  
`# yum install zabbix50-agent --enablerepo=epel`

Edit `/etc/zabbix_agentd.conf`  
_haven't managed to make encryption work yet_

```
Server=(Zabbix server ip)
ServerActive=(Zabbix server ip)
Hostname=he1xcp1
#TLSConnect=psk
#TLSAccept=psk
#TLSPSKIdentity=he1xcp1
#TLSPSKFile=/mnt/zabbix/zabbix_agentd.psk
```

Create the .psk file. Set the user and group to `zabbix`

Allow 10050/TCP on the firewall

_nope_  
`# yum install openssl11 --enablerepo=epel`

## TODO

### Encryption with PSK

> [Official Documentation](https://www.zabbix.com/documentation/6.0/en/manual/encryption/using_pre_shared_keys)

### Force refresh Proxy

> [Zabbix Forum Post](https://www.zabbix.com/forum/zabbix-troubleshooting-and-problems/363196-cannot-send-list-of-active-checks-to-ip-address-host-ip-address-hostnames-match?p=363205#post363205)

## Configuration

### SMART monitoring

Smart monitoring is available using an official template

Requirements:

```
smartmontools > 7.1
```

Additionally, the `zabbix` user needs to have permission for running `smartctl`.  
With `sudo` this can be accomplished without the user entering a password.

```
zabbix ALL=(ALL) NOPASSWD:/usr/sbin/smartctl
```

After that, add the `SMART by Zabbix agent 2` template to the host
