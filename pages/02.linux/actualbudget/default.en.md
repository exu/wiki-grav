---
title: Actual Budget
visible: false
---

[toc]

## Notes

https://github.com/actualbudget/actual-server#persisting-server-data

https://actualbudget.github.io/docs/Installing/Docker#launch-container-using-docker-command

[shroot]

```sh
podman run -d --name actualbudget -p 5006:5006 \
    -v /mnt/actualbudget:/data \
    ghcr.io/actualbudget/actual-server:latest-alpine
```

[/shroot]
