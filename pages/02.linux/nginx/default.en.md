---
title: Nginx
visible: true
media_order: content-encoding-type.png
---

[toc]

Interesting options, configurations and information about nginx.

## Compression

_NOTE: The most reliable way to check whether content is compressed, is by using the debug tools in the webbrowser. Look for the "content-encoding" header_  
![Picture shows parts of the response headers in the network tab of the firefox debug tool](content-encoding-type.png)

These are the settings used by this website to compress with gzip.  
These will suffice for most websites.

```nginx
    # Compression
    gzip on;
    gzip_vary on;
    gzip_proxied any;
    gzip_http_version 1.1;
    gzip_types text/plain text/html text/css application/json application/javascript application/x-javascript text/javascript text/xml application/xml application/rss+xml application/atom+xml application/rdf+xml;
```

> All configuration options can be found in the [official documentation](https://nginx.org/en/docs/http/ngx_http_gzip_module.html)

## Website Performance

> Google's [PageSpeed Insights](https://pagespeed.web.dev/) tool can be used to measure website performance.
