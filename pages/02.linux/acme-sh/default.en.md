---
title: ACME.SH
visible: true
---

[toc]

## Getting ACME.SH

[shuser]

```sh
git clone https://github.com/acmesh-official/acme.sh.git
cd ./acme.sh
./acme.sh --install -m [EMAIL]
```

[/shuser]

## First time ZeroSSL registration

[shuser]

```sh
.acme.sh/acme.sh --register-account -m [EMAIL]
```

[/shuser]

## Issue new certificate

Needs root to start a server on port 80

[shroot]

```sh
.acme.sh/acme.sh --issue --standalone -d [DOMAIN]
```

[/shroot]

## Issue new certificate with DNS API

> [Official Documentation](https://github.com/acmesh-official/acme.sh/wiki/dnsapi)

### Gandi

Create a personal access token with permissions to "Manage domain name technical configurations"  

[shuser]

```sh
export GANDI_LIVEDNS_TOKEN="[Personal Access Token]"
```

[/shuser]

**Warning: `export GANDI_LIVEDNS_KEY="[API KEY]"` is deprecated**  

[shuser]

```sh
.acme.sh/acme.sh --issue --dns dns_gandi_livedns -d [DOMAIN]
```

[/shuser]

## Install certificate

Make sure to create the `/etc/acme-sh/(url)` directory

[shuser]

```sh
export url=[URL] \
    && mkdir -p /etc/acme-sh/{$url} \
    && .acme.sh/acme.sh --install-cert -d $url \
        --key-file       /etc/acme-sh/{$url}/key.pem  \
        --fullchain-file /etc/acme-sh/{$url}/cert.pem \
        --reloadcmd     "sudo systemctl restart nginx"
```

[/shuser]

## Systems Service & Timer

`/etc/systemd/system/acme-sh.service`

```systemd
[Unit]
Description=Renew certificates using acme.sh
After=network-online.target

[Service]
Type=oneshot
ExecStart=(path to acme.sh) --cron --home (path to acme folder)
User=wiki

SuccessExitStatus=0 2
```

`/etc/systemd/system/acme-sh.timer`

```systemd
[Unit]
Description=Daily renewal of certificates

[Timer]
OnCalendar=daily
RandomizedDelaySec=1h
Persistent=true

[Install]
WantedBy=timers.target
```

Enable timer
[shroot]

```sh
systemctl enable --now acme-sh.timer
```

[/shroot]
