---
title: Prowlarr
visible: false
---

_NOTE: This application is still in beta. No stable release is available_

## Application

`lscr.io/linuxserver/prowlarr:develop`

```sh
podman run -d \
    --name=prowlarr \
    -p 9696:9696 \
    -v /mnt/prowlarr/config:/config \
    lscr.io/linuxserver/prowlarr:develop
```
