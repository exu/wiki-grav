---
title: "xe CLI"
visible: true
---

[toc]

## VM Operations

Show running VMs

[shroot]

```sh
xe vm-list power-state=running
```

[/shroot]

Show only the names of running VMs

[shroot]

```sh
xe vm-list power-state=running | grep "name-label"
```

[/shroot]

Poweroff all VMs currently running

[shroot]

```sh
xe vm-shutdown --multiple power-state=running
```

[/shroot]

Start VM

[shroot]

```sh
xe vm-start uuid={VM UUID}
```

[/shroot]

## Host reboot procedure

Disable the host so that no new VM can be started on this host and so that the rest of the pool knows that the host was disabled on purpose.
From command line:  
[shroot]

```sh
xe host-disable host=$(hostname)
```

[/shroot]

Migrate the VMs running on the host to other hosts in the pool, or shut them down.

Reboot or shutdown.
Can be done from XO, or from command line

[shroot]

```sh
xe host-reboot host=$(hostname)
```

[/shroot]

(After a reboot or host startup) Move VMs back to the host if appropriate. There is no need to re-enable the host: it is done automatically when it starts.

> [XCP-ng documentation](https://docs.xcp-ng.org/guides/host-reboot/)

## Snapshot management

_TODO_

## VM Limits

Check current limits

```sh
xe vm-param-get uuid={VM UUID} param-name={PARAMETER}
```

Parameters:

```
VCPUs-max: Maximum number of CPU cores
VCPUs-at-startup: Number of CPU cores initially assigned
```

Set Limits

```sh
xe vm-param-set uuid={VM UUID} {PARAMETER}={VALUE}
```

[TODO Memory limits](https://support.citrix.com/article/CTX219771/increase-ram-for-xenserver-vms-through-command-line-interface)
