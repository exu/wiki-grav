---
title: Dom0
visible: true
---

[toc]

## Firewall

The firewall configuration can be changed with an already included package.  
Call the TUI version with `system-config-firewall-tui`

The only open port will be 22/tcp for SSH Access

## SSH Access

Disable password authentication. See [ssh](/remote/ssh)

## Local ISO Storage

Using ISO Storage on "/" or subdirectories on the same partition is not really viable, as only 18GiB are assigned to this mountpoint by default.  
Instead use the local EXT mapper device. This is mounted under `/run/sr-mount/(id)`  
Create a new "ISO" directory.  
If you want to still use an easier to remember path, create a symbolic link. For example `ln -s /run/sr-mount/69d19d8e-f0dd-92d8-41bc-3d974b20f4f8/ISO/ /root/ISO`. You'll be able to use the path `/root/ISO` in the webinterface as local ISO storage.
