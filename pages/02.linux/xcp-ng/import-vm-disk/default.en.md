---
title: "Import VM disk"
visible: true
---

[toc]

## Convert to proper image format

XCP-ng only supports the `vhd`, `vmdk` or `raw` image formats.  
Use `qemu-img` to convert other formats into the desired format.

`vhd` and `vmdk` are both sparsely allocated and somewhat compressed, therefore being quicker to upload than `raw`.

To convert, use these options depending on the desired format:

| QEMU format name | Disk format |
| ---------------- | ----------- |
| vpc              | vhd         |
| vmdk             | vmdk        |
| raw              | raw         |

[shuser]

```
qemu-img convert -f qcow2 -O vmdk <image>.qcow2 <image>.vmdk
```

[/shuser]

## Import VM disk

Use Xen Orchestra to import the disk.  
The option can is found under `Import > Disk`  
Select any storage repository on the server.

![Import VM disk in XCP-ng](./import-vm-disk-xcp-ng.png)

## Create VM

The imported disk is not directly usable. Instead, we create a new VM first.  
Use whatever settings you want and create a new disk. **Make sure to choose a boot mode (BIOS/UEFI) that is compatible with the imported disk**  
After creating the VM, go into its disk management and attach the imported disk.  
The disk created with the VM can be deleted.

Finally, under the advanced tab, disable all other options besides booting from HDD and convert the VM into a template.
