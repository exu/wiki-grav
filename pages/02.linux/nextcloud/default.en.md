---
title: Nextcloud
visible: true
---

[toc]

Last modified: 2023-11-24

## Installation

Nextcloud will be using apache

[shroot]

```sh
apt install mlocate apache2 libapache2-mod-php mariadb-client mariadb-server wget unzip bzip2 curl php php-common php-curl php-gd php-mbstring php-mysql php-xml php-zip php-intl php-apcu php-redis php-bcmath php-gmp php-imagick
```

[/shroot]

Enter the MariaDB CLI as the root user

[shroot]

```sh
mariadb -u root -p
```

[/shroot]

```sql
CREATE DATABASE nextcloud;
```

For UTF8 support use this instead:

```sql
CREATE DATABASE nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
GRANT ALL ON nextcloud.* TO 'nextcloud'@'localhost' IDENTIFIED BY '{PASSWORD}';
FLUSH PRIVILEGES;
```

Exit the MariaDB prompt

Download Nextcloud into `/var/www`

[shroot]

```sh
wget https://download.nextcloud.com/server/releases/nextcloud-[VERSION].tar.bz2
tar -xf nextcloud-[VERSION].tar.bz2
```

[/shroot]

Change owner to the apache user

[shroot]

```sh
chown -Rfv www-data:www-data /var/www/nextcloud
```

[/shroot]

Create nextcloud configuration for apache

[shroot]

```sh
vi /etc/apache2/sites-available/nextcloud.conf
```

[/shroot]

Configuration file

```apacheconf
<VirtualHost *:80> # specify listen ip addresses: ADDRESS:PORT for ipv4, [ADDRESS]:PORT vor ipv6, *:80 for all
        ServerAdmin webmaster@localhost
        DocumentRoot /var/www/nextcloud
        Alias /nextcloud "/var/www/nextcloud/"

        <Directory "/var/www/nextcloud/">
                Options +FollowSymlinks
                AllowOverride All

                <IfModule mod_dav.c>
                        Dav off
                </IfModule>

                Require all granted

                SetEnv HOME /var/www/nextcloud
                SetEnv HTTP_HOME /var/www/nextcloud
        </Directory>

        ErrorLog ${APACHE_LOG_DIR}/nextcloud_error_log
        CustomLog ${APACHE_LOG_DIR}/nextcloud_access_log common
</VirtualHost>
```

Enable rewrite module

[shroot]

```
a2enmod rewrite
```

[/shroot]

Enable nextcloud and disable the default site

[shroot]

```sh
a2ensite nextcloud.conf && a2dissite 000-default.conf
```

[/shroot]

Edit `ports.conf` for apache2 to only bind the addresses you need

[shroot]

```sh
systemctl restart apache2
```

[/shroot]

### PHP Memory Cache

[shroot]

```sh
apt install php-apcu
```

[/shroot]

```php
'memcache.local' => '\OC\Memcache\APCu',
```

Enable APCu for the PHP CLI as well.

`/etc/php/8.2/cli/php.ini`

```
...
apc.enable_cli=1
```

Redis can be used for providing a distributed memcache. See the install instructions below.  

```php
'memcache.distributed' => '\OC\Memcache\Redis',
```

### Redis memcache

[shroot]

```sh
apt install redis php-redis
```

[/shroot]

If Redis is installed on the same machine, unix sockets can be used to communicate.

Enable `unixsocket` in the redis config file located under `/etc/redis/redis.conf`  
Uncomment the provided default value  
Set `unixsocketperm` to `777`, so the `www-data` webserver user can access it.  
_NOTE: There's probably a better way of doing this involving groups, but I wanted to get it working_

Add these lines to `config.php`

```php
'filelocking.enabled' => true,
'memcache.locking' => '\OC\Memcache\Redis',
'redis' => array(
    'host' => '/run/redis/redis-server.sock',
    'port' => 0,
    'timeout' => 0.0,
),
```

Restart the Redis and Apache2 services  
[shroot]

```sh
systemctl restart apache2
systemctl restart redis
```

[/shroot]

### Cron

To execute regular jobs, I personally use cron.  
Edit `crontab` as the `www-data` user.

[shroot]

```sh
su -s /bin/sh -c 'crontab -e' www-data
```

[/shroot]

Add this following line:

```
*/5 * * * * php -f [NEXTCLOUD DIR]/cron.php
```

### Configuration

The main config file is `[NEXTCLOUD DIR]/config/config.php`

#### Automatic Trash clearing

> [See this page](https://bayton.org/docs/nextcloud/nextcloud-hoarding-trash-how-to-force-automatic-removal-of-deleted-items/) for more options

This settings keeps the files for 15 days, unless drive space is getting low.  
In that case it delets them earlier.

```php
'trashbin_retention_obligation' => 'auto, 15',
```

#### Trust Proxy

This disables the warning of untrusted proxy in the webinterface.

```php
'trusted_proxies' =>
    array (
	      0 => '[PROXY IP]',
),
```

#### Trusted Domains

Array of trusted domains.

```php
'trusted_domains' =>
    array (
        0 => '[DOMAIN 1]',
        1 => '[DOMAIN 2]',
),
```

## Maintenance

### Maintenance Mode

Enable maintenance mode to prevent data inconsistencies

[shroot]

```sh
su -s /bin/sh -c 'php /var/www/nextcloud/occ maintenance:mode --on' www-data
```

[/shroot]

To disable maintenance mode again, run the same command with `--off` instead of `--on`

### Upgrade with CLI

[shroot]

```sh
su -s /bin/sh -c 'php /var/www/nextcloud/updater/updater.phar' www-data
```

[/shroot]

Always check the admin status page after an upgrade. Sometimes additional steps are needed to fully complete the upgrade

#### Missing indices in database

The missing indices can be added using `occ`

[shroot]

```sh
su -s /bin/sh -c 'php /var/www/nextcloud/occ db:add-missing-indices' www-data
```

[/shroot]

### Backup Database

Dump database to file

_NOTE: The password needs to be inserted directly after `-p` without any space_

```sh
mysqldump --single-transaction -h [SERVER] -u [USERNAME] -p[PASSWORD] [DB NAME] > nextcloud-sqlbkp_`date +"%Y%m%d"`.bak
```

> [Official documentation](https://docs.nextcloud.com/server/latest/admin_manual/maintenance/backup.html)

#### Backup Script

```sh
#!/bin/bash
set -euo pipefail

server=
username=
password=
db_name=

sudo mkdir -p /var/www/database-backup

sudo -u www-data php /var/www/nextcloud/occ maintenance:mode --on

mysqldump --single-transaction -h $server -u $username -p$password $db_name | sudo tee /var/www/database-backup/nextcloud-sqlbkp_`date +"%Y%m%d"`.bak

sudo -u www-data php /var/www/nextcloud/occ maintenance:mode --off
```

### Restore Database

```sh
mariadb -h [SERVER] -u [USERNAME] -p[PASSWORD] -e "DROP DATABASE nextcloud"
mariadb -h [SERVER] -u [USERNAME] -p[PASSWORD] -e "CREATE DATABASE nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci"
```

```sh
mariadb -h [SERVER] -u [USERNAME] -p[PASSWORD] [DB NAME] < nextcloud-sqlbkp.bak
```

> [Nextcloud documentation](https://docs.nextcloud.com/server/latest/admin_manual/maintenance/restore.html)

### Plugins

#### Memories

Install `ffmpeg` to enable thumbnail generation for video files.

## Migration

### Tips

- Allow login to the original host as root user

### Steps

- (New host) Install dependencies
- (Original host) Enable maintenance mode
- (Original host) Create database backup
- (New host) Restore database backup
- (New host) Recreate Nextcloud database user
- (New host) Copy full Nextcloud folder
  - Preserve the permissions as much as possible (see rsync command below)
  - `sudo rsync -a --progress root@172.18.50.101:/var/www/nextcloud /var/www/`
- (New host) Copy apache2 configuration file
- (New host) Enable apache2 Nextcloud site and disable default site
- (New host) Redo php limits configuration
- (New host) Upgrade Nextcloud (required for php 8.2 compatibility)

### Notes on configuration files

#### PHP

##### Apache2: `/etc/php/8.2/apache2/php.ini`

Change `memory_limit` to `1G`

Disable `post_max_size` by setting `0`  
Previously used `20G` in the old Nextcloud installation

Change `upload_max_filesize` to `20G`

Change `opcache.interned_strings_buffer` to `32`

##### CLI: `/etc/php/8.2/cli/php.ini`

Disable `post_max_size` by setting `0`  
Previously used `20G` in the old Nextcloud installation

Change `upload_max_filesize` to `20G`

## Collabora Online Container

! This chapter is unfinished

> [Docker Compose for Nextcloud + Collabora + Traefik?](https://help.nextcloud.com/t/docker-compose-for-nextcloud-collabora-traefik/127733/2)  
> [Use HTTPS with Ubuntu 22.04, apache, Nextcloud and Collabora(Docker)](https://help.nextcloud.com/t/use-https-with-ubuntu-22-04-apache-nextcloud-and-collabora-docker/142880)  
> [HowTo: Ubuntu + Docker + Nextcloud + Talk + Collabora](https://help.nextcloud.com/t/howto-ubuntu-docker-nextcloud-talk-collabora/76430)  
> [Collabora - Installation Guide](https://sdk.collaboraonline.com/docs/installation/index.html)

[shroot]

```sh
podman run -t -d --name collabora-online -p 9980:9980 \
    -e "extra_params=--o:ssl.enable=false --o:ssl.termination=true" \
    --label "io.containers.autoupdate=image" \
    docker.io/collabora/code:latest
```

[/shroot]

```nginx
server {
    listen 443 ssl;
    server_name  collabora.exu.li;

    ssl_certificate_key /etc/acme-sh/collabora.exu.li/key.pem;
    ssl_certificate /etc/acme-sh/collabora.exu.li/cert.pem;

    # static files
    location ^~ /browser {
      proxy_pass http://172.18.50.101:9980;
      proxy_set_header Host $http_host;
    }

    # WOPI discovery URL
    location ^~ /hosting/discovery {
      proxy_pass http://172.18.50.101:9980;
      proxy_set_header Host $http_host;
    }

    # Capabilities
    location ^~ /hosting/capabilities {
      proxy_pass http://172.18.50.101:9980;
      proxy_set_header Host $http_host;
    }

    # main websocket
    location ~ ^/cool/(.*)/ws$ {
      proxy_pass http://172.18.50.101:9980;
      proxy_set_header Upgrade $http_upgrade;
      proxy_set_header Connection "Upgrade";
      proxy_set_header Host $http_host;
      proxy_read_timeout 36000s;
    }

    # download, presentation and image upload
    location ~ ^/(c|l)ool {
      proxy_pass http://172.18.50.101:9980;
      proxy_set_header Host $http_host;
    }

    # Admin Console websocket
    location ^~ /cool/adminws {
      proxy_pass http://172.18.50.101:9980;
      proxy_set_header Upgrade $http_upgrade;
      proxy_set_header Connection "Upgrade";
      proxy_set_header Host $http_host;
      proxy_read_timeout 36000s;
    }
}
```

## Onlyoffice Container

> This procedure is incomplete. See these links for the additional configurations necessary.  
> [jiriks74 Docker-DocumentServer](https://gitea.stefka.eu/jiriks74/Docker-DocumentServer#setting-up-secret-key-with-nextcloud)  
> [Onlyoffice-Nextcloud issue 601](https://github.com/ONLYOFFICE/onlyoffice-nextcloud/issues/601#issuecomment-1089929204)  
> [Onlyoffice-Nextcloud issue 153](https://github.com/ONLYOFFICE/onlyoffice-nextcloud/issues/153#issuecomment-544900501)

Integrating onlyoffice, requires setting the correct Content Security Policy headers on the webserver. Using CSP also introduces blockages in Nextcloud that have to be fixed. The console view is your friend for finding every issue.  
For my installation, the headers needed to be set like this.

```
Content-Security-Policy "default-src 'self' 'unsafe-inline' 'unsafe-eval' data: [ONLYOFFICE DOMAIN NAME];"
```

[shroot]

```sh
podman run -itd --name onlyoffice -p 8080:80 \
    -e JWT_ENABLED="true" \
    -e JWT_SECRET="[SECRET KEY]" \
    -e JWT_HEADER="AuthorizationJwt" \
    docker.io/onlyoffice/documentserver
```

[/shroot]

> [Installing ONLYOFFICE Docs Community Edition for Docker on a local server](https://helpcenter.onlyoffice.com/installation/docs-community-install-docker.aspx)  
> [About the ONLYOFFICE and Nextcloud integration](https://helpcenter.onlyoffice.com/integration/gettingstarted-nextcloud.aspx)  
> [Using ONLYOFFICE Docs behind the proxy](https://helpcenter.onlyoffice.com/installation/docs-community-proxy.aspx)
