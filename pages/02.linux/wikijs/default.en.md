---
title: WikiJS
visible: true
---

[toc]

> I'm not using WikiJS anymore. This article might be out of date

`# apt install nginx podman nodejs`

## Preparation

Create a new network for the database and wikijs  
`$ podman network create wikijs`

## Database setup

`# podman pull docker://postgres`

```sh
podman run -p 127.0.0.1:5432:5432 --name wikijsdb \
    -e POSTGRES_PASSWORD=wikijs \
    -e PGDATA=/var/lib/postgresql/data/pgdata \
    -v /mnt/postgres/wikijsdb:/var/lib/postgresql/data \
    -d docker.io/postgres:15
```

`# podman exec -it wikijsdb bash`

`# psql -U postgres`

Create database used by wikijs

```sql
CREATE DATABASE wikijs;
```

### Systemd Service

Generate the systems service file following the [podman guide](/linux/services/podman)

## Wiki.JS Setup

```sh
cd /var
wget https://github.com/Requarks/wiki/releases/download/(version)/wiki-js.tar.gz
mkdir wiki
tar xzf wiki-js.tar.gz -C ./wiki
cd ./wiki
```

Move default config  
`# mv config.sample.yml config.yml`

```
#######################################################################
# Wiki.js - CONFIGURATION                                             #
#######################################################################
# Full documentation + examples:
# https://docs.requarks.io/install

# ---------------------------------------------------------------------
# Port the server should listen to
# ---------------------------------------------------------------------

port: 3000

# ---------------------------------------------------------------------
# Database
# ---------------------------------------------------------------------
# Supported Database Engines:
# - postgres = PostgreSQL 9.5 or later
# - mysql = MySQL 8.0 or later (5.7.8 partially supported, refer to docs)
# - mariadb = MariaDB 10.2.7 or later
# - mssql = MS SQL Server 2012 or later
# - sqlite = SQLite 3.9 or later

db:
  type: postgres

  # PostgreSQL / MySQL / MariaDB / MS SQL Server only:
  host: localhost
  port: 5432
  user: postgres
  pass: wikijs
  db: wikijs
  ssl: false

  # Optional - PostgreSQL / MySQL / MariaDB only:
  # -> Uncomment lines you need below and set `auto` to false
  # -> Full list of accepted options: https://nodejs.org/api/tls.html#tls_tls_createsecurecontext_options
  sslOptions:
    auto: true
    # rejectUnauthorized: false
    # ca: path/to/ca.crt
    # cert: path/to/cert.crt
    # key: path/to/key.pem
    # pfx: path/to/cert.pfx
    # passphrase: xyz123

  # SQLite only:
  storage: path/to/database.sqlite

#######################################################################
# ADVANCED OPTIONS                                                    #
#######################################################################
# Do not change unless you know what you are doing!

# ---------------------------------------------------------------------
# SSL/TLS Settings
# ---------------------------------------------------------------------
# Consider using a reverse proxy (e.g. nginx) if you require more
# advanced options than those provided below.

ssl:
  enabled: false
  port: 3443

  # Provider to use, possible values: custom, letsencrypt
  provider: custom

  # ++++++ For custom only ++++++
  # Certificate format, either 'pem' or 'pfx':
  format: pem
  # Using PEM format:
  key: path/to/key.pem
  cert: path/to/cert.pem
  # Using PFX format:
  pfx: path/to/cert.pfx
  # Passphrase when using encrypted PEM / PFX keys (default: null):
  passphrase: null
  # Diffie Hellman parameters, with key length being greater or equal
  # to 1024 bits (default: null):
  dhparam: null

  # ++++++ For letsencrypt only ++++++
  domain: wiki.yourdomain.com
  subscriberEmail: admin@example.com

# ---------------------------------------------------------------------
# Database Pool Options
# ---------------------------------------------------------------------
# Refer to https://github.com/vincit/tarn.js for all possible options

pool:
  # min: 2
  # max: 10

# ---------------------------------------------------------------------
# IP address the server should listen to
# ---------------------------------------------------------------------
# Leave 0.0.0.0 for all interfaces

bindIP: 0.0.0.0

# ---------------------------------------------------------------------
# Log Level
# ---------------------------------------------------------------------
# Possible values: error, warn, info (default), verbose, debug, silly

logLevel: info

# ---------------------------------------------------------------------
# Offline Mode
# ---------------------------------------------------------------------
# If your server cannot access the internet. Set to true and manually
# download the offline files for sideloading.

offline: false

# ---------------------------------------------------------------------
# High-Availability
# ---------------------------------------------------------------------
# Set to true if you have multiple concurrent instances running off the
# same DB (e.g. Kubernetes pods / load balanced instances). Leave false
# otherwise. You MUST be using PostgreSQL to use this feature.

ha: false

# ---------------------------------------------------------------------
# Data Path
# ---------------------------------------------------------------------
# Writeable data path used for cache and temporary user uploads.
dataPath: ./data
```

Don't forget to open permissions so the systemd service can run the server

```sh
useradd -m wiki
chown wiki:wiki -R /var/wiki
```

Run server directly:  
`$ node server`

## Systemd service

Put this under `/etc/systemd/system/wiki.service`

```systemd
[Unit]
Description=Wiki.js
After=network.target
Wants=container-wikijsdb.service

[Service]
Type=simple
ExecStart=/usr/bin/node server
Restart=always
# Consider creating a dedicated user for Wiki.js here:
#User=nobody
User=wiki
Environment=NODE_ENV=production
WorkingDirectory=/var/wiki

[Install]
WantedBy=multi-user.target
```

```sh
systemctl daemon-reload
systemctl enable --now wiki
```

## Nginx config

_Replace "IPV4" and "IPV6"_

```nginx
server {
    server_name DOMAIN_NAME;

    # Security / XSS Mitigation Headers
    add_header X-Frame-Options "SAMEORIGIN";
    add_header X-XSS-Protection "1; mode=block";
    add_header X-Content-Type-Options "nosniff";

    location = / {
        return 302 https://$host/web/;
    }

    location / {
        # Proxy main traffic
        proxy_pass http://127.0.0.1:3000;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-Protocol $scheme;
        proxy_set_header X-Forwarded-Host $http_host;
    }

    listen [IPV6]:443 ssl; #set ipv6 address
    # acme.sh
    ssl_certificate_key /etc/acme-sh/DOMAIN_NAME/key.pem;
    ssl_certificate /etc/acme-sh/DOMAIN_NAME/cert.pem;
    # letsencrypt
    #ssl_certificate /etc/letsencrypt/live/DOMAIN_NAME/fullchain.pem;
    #ssl_certificate_key /etc/letsencrypt/live/DOMAIN_NAME/privkey.pem;
    #include /etc/letsencrypt/options-ssl-nginx.conf;
    #ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem;
}

server {
    if ($host = DOMAIN_NAME) {
        return 301 https://$host$request_uri;
    }

    listen [IPV6]:80; #set ipv6 address
    server_name DOMAIN_NAME;
    return 404;
}
```

Enable config  
`# ln -s /etc/nginx/sites-available/(config) /etc/nginx/sites-enabled`

Restart nginx  
`# systemctl restart nginx`

## Wiki Settings

### Storage with git

Create a home directory for the wiki user if you haven't used "-m" when creating the user.  
**Make sure not to have a "/" after the directory you want for your user**

```sh
mkdir /home/wiki
chown wiki:wiki -R /home/wiki
usermod -d /home/wiki wiki
```

Create ssh key as wiki user  
`$ ssh-keygen -t ed25519 -C wiki`

- DB - PostgreSQL used as Search Engine

## Update Wiki

Download and install the latest release with these steps

```sh
systemctl stop wiki
cd /var
wget https://github.com/Requarks/wiki/releases/download/(version)/wiki-js.tar.gz
```

This is to ensure we have a known good version to go back to in case something goes wrong

```sh
mv wiki wiki-old
mkdir wiki
tar xzf wiki-js.tar.gz -C ./wiki
cp wiki-old/config.yml wiki/
chown wiki:wiki -R /var/wiki
systemctl start wiki
```

## Database Backup

`# podman exec (container name) pg_dump (database name) -U (database user) -F c > wikibackup.dump`

## Database Restore

**The wiki has to be installed fully, but not yet configured**  
_Also works for transfering wiki from one server to another_  
Stop the database and wiki

Drop the existing database and restore from the database

```sh
podman exec -it (container name) dropdb -U (database user) (database name)
podman exec -it (container name) createdb -U (database user) (database name)
cat ~/wikibackup.dump | docker exec -i (container name) pg_restore -U (database user) -d (database name)
```

Start the database and wiki again
