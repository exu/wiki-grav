---
title: Podman
visible: true
---

[toc]

## Generate systemd service

Create a container the normal way

Using this container as a reference, you can generate a systemd service file

```sh
podman generate systemd --new --name --files (container)
```

Remove your old container

```
podman container rm (container)
cp container-(container).service /etc/systemd/system/
systemctl daemon-reload
systemctl enable --now container-(container)
```

The container should now be running just as before

## Auto-Update container

The command to update containers configured for auto-update is `# podman auto-update`

Add `--label "io.containers.autoupdate=image"` to the `ExecStart=/usr/bin/podman ...` line in the service file you generated  
Make sure to use, for example, `docker.io/` instead of `docker://` as the source of the image

Reload and restart

```sh
systemctl daemon-reload
systemctl enable --now container-(container)
```

If you want to manually run updates for the configured containers, use this command:  
`# podman auto-update`

### Auto-Update timer

To truly automate your updates, enable the included timer  
`# systemctl enable --now podman-auto-update.timer`

### Check update log

The update logs are kept in the `podman-auto-update` service  
`$ journalctl -eu podman-auto-update`

## Prune images service and timer

`/etc/systemd/system/podman-image-prune.service`

```systemd
[Unit]
Description=Podman image-prune service

[Service]
Type=oneshot
ExecStart=/usr/bin/podman image prune -f

[Install]
WantedBy=multi-user.target
```

`/etc/systemd/system/podman-image-prune.timer`

```systemd
[Unit]
Description=Podman image-prune timer

[Timer]
OnCalendar=weekly
Persistent=true

[Install]
WantedBy=timers.target
```

```sh
systemctl daemon-reload
systemctl enable --now podman-image-prune.timer
```

> [Documentation](https://docs.podman.io/en/latest/markdown/podman-image-prune.1.html)
