---
title: Komga
visible: true
---

[toc]

> I'm not using Komga anymore. This article might be out of date

## Create directories

```sh
mkdir -p /var/komga/{config,content}
mkdir -p /var/komga/content/{manga,books,tech}
```

## Run Komga

```sh
# podman run --name komga -p 8080:8080 \
    -v /var/komga/config:/config \
    -v /var/komga/content:/content \
    --restart unless-stopped \
    -d docker.io/gotson/komga:latest
```

## Nginx Config

```nginx
server {
    server_name komga.exu.li;

    # Security / XSS Mitigation Headers
    add_header X-Frame-Options "SAMEORIGIN";
    add_header X-XSS-Protection "1; mode=block";
    add_header X-Content-Type-Options "nosniff";
    add_header Strict-Transport-Security "max-age=31536000; includeSubDomains";

    location / {
        # Proxy main traffic
        proxy_pass http://172.16.53.100:8080;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-Protocol $scheme;
        proxy_set_header X-Forwarded-Host $http_host;
    }

    listen *:443 ssl http2; #set ipv6 address
    ssl_certificate_key /etc/acme-sh/komga.exu.li/key.pem;
    ssl_certificate /etc/acme-sh/komga.exu.li/cert.pem;
}

server {
    if ($host = komga.exu.li) {
        return 301 https://$host$request_uri;
    }

    listen *:80; #set ipv6 address
    server_name komga.exu.li;
    return 404;
}
```

## Systemd Service

> See [Podman](/linux/podman#generate-systemd-service) to generate a service file.
