---
title: "Bind DNS"
visible: false
---

[toc]

## Installation

### Debian

[shroot]

```sh
apt install bind9
```

[/shroot]

Generate keys for different views

[shroot]

```sh
tsig-keygen -a hmac-sha256 [VIEW]
```

[/shroot]

https://jensd.be/160/linux/split-horizon-dns-masterslave-with-bind

_TODO_
