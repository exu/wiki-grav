---
title: Tdarr
visible: true
---

[toc]

Ports:

```
8265: Port for webUI
8266: Port to connect Nodes
```

## Server with integrated Node

```sh
podman run -it --name tdarr \
    -p 8265:8265 \
    -p 8266:8266 \
    -v /mnt/tdarr/server/:/app/server \
    -v /mnt/tdarr/configs/:/app/configs \
    -v /mnt/tdarr/logs/:/app/logs \
    -v /mnt/tdarr/tmp/:/temp \
    -v /mnt/Media/:/media \
    -e "internalNode=true" \
    -e "nodeID=localnode" \
    --log-opt max-size=10m \
    --log-opt max-file=5 \
    -d gitea.exu.li/realstickman/tdarr-ffmpeg5.1:latest
```

## Server only

```sh
podman run -it --name tdarr-server \
    -p 8265:8265 \
    -p 8266:8266 \
    -v /mnt/tdarr/server/:/app/server \
    -v /mnt/tdarr/configs/:/app/configs \
    -v /mnt/tdarr/logs/:/app/logs \
    -v /mnt/tdarrshare/cache:/cache \
    -v /mnt/tdarrshare/media:/media \
    -d gitea.exu.li/realstickman/tdarr-server-ffmpeg5.1:latest
```

## Node

```sh
podman run -it --name tdarr-node \
    -e nodeID={NAME} \
    -e serverIP={IP} \
    -e serverPort=8266 \
    -v /mnt/tdarr/configs/:/app/configs \
    -v /mnt/tdarr/logs/:/app/logs \
    -v /mnt/tdarrshare/cache:/cache \
    -v /mnt/tdarrshare/media:/media \
    -d gitea.exu.li/realstickman/tdarr-node-ffmpeg5.1:latest
```
