---
title: Hedgedoc
visible: true
---

[toc]

## Podman

### Network and Pod

```sh
podman network create net_hedgedoc
podman pod create --name pod_hedgedoc --network net_hedgedoc -p 127.0.0.1:5432:5432 -p 3005:3000
```

### Database

```sh
# podman run --name hedgedocdb \
    -e POSTGRES_PASSWORD=hedgedoc \
    -e PGDATA=/var/lib/postgresql/data/pgdata \
    -v /mnt/postgres:/var/lib/postgresql/data \
    --pod pod_hedgedoc \
    -d docker.io/postgres:14
```

```sh
podman exec -it hedgedocdb bash
psql -U postgres
```

Create database used by hedgedoc

```sql
CREATE DATABASE hedgedocdb;
```

### Application

```sh
# podman run -d --name hedgedoc \
    -e CMD_DB_URL=postgres://postgres:hedgedoc@localhost:5432/hedgedocdb \
    -e CMD_DOMAIN=(url) \
    -e CMD_PROTOCOL_USESSL=true \
    -e CMD_ALLOW_ANONYMOUS=false \
    -e CMD_ALLOW_ANONYMOUS_EDITS=true \
    -e CMD_DEFAULT_PERMISSION=private \
    -e CMD_ALLOW_EMAIL_REGISTER=false \
    -v /mnt/hedgedoc:/hedgedoc/public/uploads \
    --pod pod_hedgedoc \
    quay.io/hedgedoc/hedgedoc:latest
```

## Create user

Because `CMD_ALLOW_EMAIL_REGISTER` is set to `false`, registration of new users has to be done through the CLI interface using `bin/manage_users` in the container.

```sh
podman exec -it hedgedocdb bash
bin/manage_users --add (email)
```

## Nginx config

```nginx
server {
    server_name SERVER_NAME;

    # Security / XSS Mitigation Headers
    add_header X-Frame-Options "SAMEORIGIN";
    add_header X-XSS-Protection "1; mode=block";
    add_header X-Content-Type-Options "nosniff";
    add_header Strict-Transport-Security "max-age=31536000; includeSubDomains";

    location / {
        # Proxy main traffic
        proxy_pass http://(SERVER);
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-Protocol $scheme;
        proxy_set_header X-Forwarded-Host $http_host;
    }

    location /socket.io/ {
        proxy_pass http://(SERVER);
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-Protocol $scheme;
        proxy_set_header X-Forwarded-Host $http_host;
    }

    listen *:443 ssl http2;
    ssl_certificate_key /etc/acme-sh/SERVER_NAME/key.pem;
    ssl_certificate /etc/acme-sh/SERVER_NAME/cert.pem;
}

server {
    if ($host = SERVER_NAME) {
        return 301 https://$host$request_uri;
    }

    listen *:80;
    server_name SERVER_NAME;
    return 404;
}
```
