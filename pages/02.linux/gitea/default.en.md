---
title: Gitea
visible: true
---

[toc]

## Pre-Setup

Create a gitea user

[shroot]

```sh
useradd -m git
mkdir /etc/gitea
chown git:git -R /etc/gitea
```

[/shroot]

Create the .ssh directory for the git user

[shuser]

```sh
sudo -u git mkdir -p /home/git/.ssh
```

[/shuser]

Get the user id of git with `id git`

## Podman

### Network and Pod

[shroot]

```sh
podman network create net_gitea
podman pod create --name pod_gitea --network net_gitea -p 127.0.0.1:5432:5432 -p 3000:3000 -p 127.0.0.1:2222:22
```

[/shroot]

#### Port Mappings

```
5432 (localhost): Postgres Database
3000: Gitea WebUI
2222 (localhost): Gitea SSH
```

### Database

[shroot]

```sh
podman run --name giteadb \
    -e PGDATA=/var/lib/postgresql/data/pgdata \
    -e POSTGRES_USER=gitea \
    -e POSTGRES_PASSWORD=gitea \
    -e POSTGRES_DB=gitea \
    -v /mnt/postgres:/var/lib/postgresql/data \
    --pod pod_gitea \
    -d docker.io/postgres:14
```

[/shroot]

### Application

[shroot]

```sh
podman run --name gitea \
    -e USER_UID=[UID] \
    -e USER_GID=[GID] \
    -e GITEA__database__DB_TYPE=postgres \
    -e GITEA__database__HOST=giteadb:5432 \
    -e GITEA__database__NAME=gitea \
    -e GITEA__database__USER=gitea \
    -e GITEA__database__PASSWD=gitea \
    -v /mnt/gitea:/data \
    -v /home/git/.ssh/:/data/git/.ssh \
    -v /etc/timezone:/etc/timezone:ro \
    -v /etc/localtime:/etc/localtime:ro \
    --pod pod_gitea \
    -d docker.io/gitea/gitea:latest
```

[/shroot]

**NOTE:** gitea's /data directory must not contain permissions too open. Otherwise the SSH redirection set up below will fail.  
`0750` for directories and `0640` is known to work.

The next few lines are used to set up ssh-redirection to gitea if it is used to clone a repo.

> See also the [official documentation](https://docs.gitea.io/en-us/install-with-docker/#sshing-shim-with-authorized_keys)

Create SSH Keys for gitea

[shuser]

```sh
sudo -u git ssh-keygen -t rsa -b 4096 -C "Gitea Host Key"
sudo -u git cat /home/git/.ssh/id_rsa.pub | sudo -u git tee -a /home/git/.ssh/authorized_keys
sudo -u git chmod 600 /home/git/.ssh/authorized_keys

cat <<"EOF" | sudo tee /usr/local/bin/gitea
#!/bin/sh
ssh -p 2222 -o StrictHostKeyChecking=no git@127.0.0.1 "SSH_ORIGINAL_COMMAND=\"$SSH_ORIGINAL_COMMAND\" $0 $@"
EOF

chmod +x /usr/local/bin/gitea
```

[/shuser]

We've now finished setting up the ssh-redirection.  
After that, connect to the Server on port 3000 to finish the installation  
The first registered user will be made admin

## Management CLI

Gitea comes with a management cli. To access it, change into the Container first and su into the user "git".

[shroot]

```sh
podman exec -it gitea bash
su git
```

[/shroot]

### User Management

List users:

[shroot]

```sh
gitea admin user list
```

[/shroot]

Change user password:

[shroot]

```sh
gitea admin user change-password -u [USER] -p [PASSWORD]
```

[/shroot]

## Package Management

### Container Registry

Gitea comes with a built-in container registry.

#### Login

[shuser]

```sh
podman login gitea.exu.li
```

[/shuser]

#### Push image

[shuser]

```sh
podman push [IMAGE ID] docker://gitea.exu.li/[OWNER]/[IMAGE]:[TAG]
```

[/shuser]
