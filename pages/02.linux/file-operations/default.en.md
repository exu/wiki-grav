---
title: "File Operations"
visible: true
---

[toc]

## Workings of file permissions

### Change permissions

To change file permissions use `chmod (-R) XXX [PATH]`

XXX signify the permissions for the file's owner/group/others respectively

Each X goes from 0 to 7.  
What each number means can be easily calculated by looking at what the individual bit values mean.

```
0 -> No Permission
4 -> Read Permission
2 -> Write Permission
1 -> Execute Permission
```

A value of 5 therefor gives the permissions "Read" and "Execute".

_To enter a folder, you need the read as well as the execute permission!_

### Change user and group

Use `chown` to change the owner and group of a file or directory.  
If you only want to change the user or the group, only specify the part left or right of `:` respectively.

Example:  
`chown (-R) [OWNER]:[GROUP] [PATH]`

## Find biggest files

`find . -type f -print0 | xargs -0 du -s | sort -n | tail -[AMOUNT] | cut -f2 | xargs -I{} du -sh {}`

`find . -type f -printf "%s %p\n" | sort -nr | head -5`
