---
title: "Woodpecker CI"
visible: true
---

[toc]

This page details installation instructions for Woodpecker CI with a connection to a self-hosted [Gitea](/linux/gitea) instance.  
Other Git Forges are also available, [learn more here](https://woodpecker-ci.org/docs/administration/forges/overview).  
Woodpecker will be deployed as a container.

## Podman

### Network and Pod

Multiple containers will be created. To separate them from other containers while also simplifying access between member containers, a container network and pod are created.

```sh
podman network create net_woodpecker
podman pod create --name pod_woodpecker --network net_woodpecker -p 8000:8000 -p 9000:9000
```

#### Port Mappings

Woodpecker CI uses these ports for outside communication by default.

```
8000: Woodpecker HTTP listener, Configurable with "WOODPECKER_SERVER_ADDR"
9000: Woodpecker gRPC listener, Configurable with "WOODPECKER_GRPC_ADDR"
```

### Database

The database container is standard PostgreSQL.  

```sh
podman run --name woodpeckerdb \
    -e PGDATA=/var/lib/postgresql/data/pgdata \
    -e POSTGRES_USER=woodpecker \
    -e POSTGRES_PASSWORD=woodpecker \
    -e POSTGRES_DB=woodpecker \
    -v /mnt/postgres-woodpecker:/var/lib/postgresql/data \
    --pod pod_woodpecker \
    -d docker.io/postgres:14
```

### Application server

> [Official Documentation](https://woodpecker-ci.org/docs/administration/server-config)

These options apply regardless of the Git Forge you want to use.  

```sh
podman run --name woodpecker-server -t \
    -e WOODPECKER_HOST=https://(hostname/ip address) \
    -e WOODPECKER_ADMIN=RealStickman \
    -e WOODPECKER_OPEN=false \
    -e WOODPECKER_AGENT_SECRET=(shared secret for server and agents) \
    -e WOODPECKER_DATABASE_DRIVER=postgres \
    -e WOODPECKER_DATABASE_DATASOURCE='postgres://(user):(password)@woodpeckerdb:5432/(database)?sslmode=disable' \
    -v /mnt/woodpecker:/var/lib/woodpecker/ \
    --pod pod_woodpecker \
    -d docker.io/woodpeckerci/woodpecker-server:latest
```

If `WOODPECKER_OPEN` is set to `true`, any user present on the connected git server could log in to woodpecker.  
If one wanted to add a user manually: `$ woodpecker-cli user add`

Generate `WOODPECKER_AGENT_SECRET` with this command:  
`$ openssl rand -hex 32`

#### Gitea

> [Documentation](https://woodpecker-ci.org/docs/administration/vcs/gitea)

Add these environment variables to enable Woodpecker for a gitea server.

```sh
    -e WOODPECKER_GITEA=true \
    -e WOODPECKER_GITEA_URL=https://(gitea url) \
    -e WOODPECKER_GITEA_CLIENT='(oauth client id)' \
    -e WOODPECKER_GITEA_SECRET='(oauth client secret)' \
    -e WOODPECKER_GITEA_SKIP_VERIFY=false \
```

I run gitea and woodpecker behind an OPNsense firewall. The default NAT configuration is blocking the connection due to a suspected DNS rebind attack.  
A simple workaround is adding an override rule in OPNsense under `Services > Unbound DNS > Overrides`.

> [Helpful Reddit post](https://www.reddit.com/r/OPNsenseFirewall/comments/lrmtsz/a_potential_dns_rebind_attack/)

### Application agent

> [Official Documentation](https://woodpecker-ci.org/docs/administration/agent-config)

The Woodpecker agent must be deployed as a separate container.  
It needs access to the docker socket to spawn new container processes on the host.  
Podman isn't well supported currently, so I'll be using Docker for this one container instead (See below).

```sh
docker run --name woodpecker-agent -t \
    -e WOODPECKER_SERVER=(url/ip):(grpc port) \
    -e WOODPECKER_AGENT_SECRET=(shared secret for server and agents) \
    -e WOODPECKER_HOSTNAME=(agent hostname, def: empty) \
    -e WOODPECKER_MAX_PROCS=(number of parallel builds, def: 1) \
    -e WOODPECKER_GRPC_SECURE=true \
    -v /var/run/docker.sock:/var/run/docker.sock \
    --restart unless-stopped \
    -d docker.io/woodpeckerci/woodpecker-agent:latest
```

#### Podman Socket Notes

Podman has support for using sockets since version 3.4.0.  
_Recommended by Woodpecker is at least Podman 4.0_  
[Podman socket activation](https://github.com/containers/podman/blob/main/docs/tutorials/socket_activation.md)

[Woodpecker note on using Podman](https://github.com/woodpecker-ci/woodpecker/blob/master/docs/docs/30-administration/22-backends/10-docker.md#podman-support)  
[Woodpecker issue about Podman](https://github.com/woodpecker-ci/woodpecker/issues/85)  
[Woodpecker PR for Podman backend](https://github.com/woodpecker-ci/woodpecker/pull/305)
