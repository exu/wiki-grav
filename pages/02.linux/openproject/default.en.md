---
title: Openproject
visibility: false
---

[toc]

## Application

_NOTE: Openproject does not provide a default "latest" tag. Specifying the tag is required!_

```sh
podman run -p 8080:80 --name openproject \
    -e OPENPROJECT_HOST__NAME=openproject.exu.li \
    -e OPENPROJECT_SECRET_KEY_BASE=<secret> \
    -v /mnt/openproject/pgdata:/var/openproject/pgdata \
    -v /mnt/openproject/assets:/var/openproject/assets \
    -d docker.io/openproject/community:12
```
