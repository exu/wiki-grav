---
title: Jellyfin
visible: true
---

[toc]

## Container Image

```sh
# podman run -d --name jellyfin -p 8096:8096 \
    -v /mnt/jellyfin/cache:/cache \
    -v /mnt/jellyfin/config:/config \
    -v /mnt/media:/media \
    docker.io/jellyfin/jellyfin:latest
```

## Apt Packate

```sh
apt install nginx apt-transport-https
wget -O - https://repo.jellyfin.org/jellyfin_team.gpg.key | apt-key add -
echo "deb [arch=$( dpkg --print-architecture )] https://repo.jellyfin.org/$( awk -F'=' '/^ID=/{ print $NF }' /etc/os-release ) $( awk -F'=' '/^VERSION_CODENAME=/{ print $NF }' /etc/os-release ) main" | tee /etc/apt/sources.list.d/jellyfin.list
apt update
apt install jellyfin
```

## Nginx

Create a new nginx configuration file in `/etc/nginx/sites-available`  
_Make sure to replace "DOMAIN_NAME" with your domain name_  
_Replace "IPV4" with an ipv4 address and "IPV6" with an ipv6 address_

```nginx
server {
    server_name DOMAIN_NAME;

    # use a variable to store the upstream proxy
    # in this example we are using a hostname which is resolved via DNS
    # (if you aren't using DNS remove the resolver line and change the variable to point to an IP address e.g `set $jellyfin 127.0.0.1`)
    set $jellyfin 127.0.0.1;
    #resolver 127.0.0.1 valid=30;

    # Security / XSS Mitigation Headers
    add_header X-Frame-Options "SAMEORIGIN";
    add_header X-XSS-Protection "1; mode=block";
    add_header X-Content-Type-Options "nosniff";

    location = / {
        return 302 https://$host/web/;
    }

    location / {
        # Proxy main Jellyfin traffic
        proxy_pass http://$jellyfin:8096;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-Protocol $scheme;
        proxy_set_header X-Forwarded-Host $http_host;

        # Disable buffering when the nginx proxy gets very resource heavy upon streaming
        proxy_buffering off;
    }

    # location block for /web - This is purely for aesthetics so /web/#!/ works instead of having to go to /web/index.html/#!/
    location = /web/ {
        # Proxy main Jellyfin traffic
        proxy_pass http://$jellyfin:8096/web/index.html;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-Protocol $scheme;
        proxy_set_header X-Forwarded-Host $http_host;
    }

    location /socket {
        # Proxy Jellyfin Websockets traffic
        proxy_pass http://$jellyfin:8096;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-Protocol $scheme;
        proxy_set_header X-Forwarded-Host $http_host;
    }
		listen IPV4:443 ssl; #set ipv4
    listen [IPV6]:443 ssl; #set ipv6 address
    ssl_certificate /etc/letsencrypt/live/DOMAIN_NAME/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/DOMAIN_NAME/privkey.pem;
    include /etc/letsencrypt/options-ssl-nginx.conf;
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem;
}

server {
    if ($host = DOMAIN_NAME) {
        return 301 https://$host$request_uri;
    }
		listen IPV4:80; #set ipv4
    listen [IPV6]:80; #set ipv6 address
    server_name DOMAIN_NAME;
    return 404;
}
```

Enable the config and restart nginx

```sh
ln -s /etc/nginx/sites-available/(config) /etc/nginx/sites-enabled/
systemctl restart nginx
```

## Migrate Authentication

There are two possible ways to migrate user accounts to a different authentication backend.  
I have personally used the first option, but the second option might work as well

### Option one: Match Jellyfin Username

The first option is simply matching the Jellyfin username to the username, UID or other provided by the authentication backend.  
After setting the username, simply switching the authentication provider allowed logging in with the new authentication backend.

### Option two: Copy Progress from library.db

For the second option, a new user account is created.  
The progress of the old user account will be copied to this newly created account.

A guide for the process can be found on Reddit.

> [How to transfer users from one instance of Jellyfin to another?](https://old.reddit.com/r/jellyfin/comments/ejat65/how_to_transfer_users_from_one_instance_of/)
