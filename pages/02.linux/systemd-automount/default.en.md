---
title: "Systemd Automount"
visible: true
---

[toc]

Systemd can be used to mount filesystems not only on boot (simple `.mount` file), but also on request by any process. (`.automount` file)

## Mount file

The `.mount` file should be placed in `/etc/systemd/system`  
**NOTE: The filename must be (mountpoint).mount with slashes `/` being replaced with dashes `-`**  
Example: `/mnt/target` --> `mnt-target.mount`

Here's an example `.mount` file for a CIFS share

```systemd
[Unit]
Description=cifs mount

[Mount]
What=//(url/ip)/(sharename)
Where=/(target mountpoint)
Type=cifs
Options=defaults,username=(user),password=(password),file_mode=0640,dir_mode=0750,iocharset=utf8,uid=(local uid),gid=(local gid)

[Install]
WantedBy=multi-user.target
```

## Automount file

The corresponding `.automount` file needs to have the same name as its `.mount` file  
Example: `mnt-target.mount` and `mnt-target.automount`

```systemd
[Unit]
Description=cifs automount

[Automount]
Where=/(target mountpoint)

[Install]
WantedBy=multi-user.target
```

Enable the `.automount` file to mount the filesystem when necessary  
`# systemctl enable (target-mount).automount`

## Service - require mount

Other services that depend on the filesystem being mounted might have issues with the built-in automounting.  
In these cases, the option `RequiresMountsFor=` can be set under the `[Unit]` configuration to ensure a path is mounted.  
Paths are space separated

```systemd
[Unit]
...
RequiresMountsFor=[PATH 1] [PATH 2]
```

> [Systemd RequiresMountsFor documentation](https://www.freedesktop.org/software/systemd/man/systemd.unit.html#RequiresMountsFor=)
