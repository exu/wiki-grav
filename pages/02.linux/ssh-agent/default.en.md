---
title: "SSH Agent"
visible: true
---

[toc]

Autostarting an ssh-agent service

## Systemd Service

A local service works for this. For example `~/.config/systemd/user/ssh-agent.service`

```systemd
[Unit]
Description=SSH key agent

[Service]
Type=simple
Environment=SSH_AUTH_SOCK=%t/ssh-agent.socket
ExecStart=/usr/bin/ssh-agent -D -a $SSH_AUTH_SOCK

[Install]
WantedBy=default.target
```

Enable the systemd service  
`systemctl --user enable --now ssh-agent`

## Shell environment variable

The shell needs to know about the ssh-agent. In the case of fish, add this snippet to your config.  
`set SSH_AUTH_SOCK /run/user/1000/ssh-agent.socket; export SSH_AUTH_SOCK`

## SSH config

Modify the `~/.ssh/config` to add new keys automatically.

```
AddKeysToAgent  yes
```
