---
title: Data Recovery
visible: false
---

[toc]

## Create disk image

### ddrescue

This command copies as much as possible from a failing drive into a disk image

[shroot]

```
ddrescue -n /dev/sdX copy.img rescue.map
```

[/shroot]

The second round uses multiple tries in attempts to also rescue previously identified bad blocks.

[shroot]

```
ddrescue -d -r3 -n /dev/sdX copy.img rescue.map
```

[/shroot]

> [ArchWiki - Disk cloning](https://wiki.archlinux.org/title/Disk_cloning#Using_ddrescue)  
> [Unix StackExchange - Please help me rescueing a failing hard drive](https://unix.stackexchange.com/a/643419)

## Recover Partition Table

*TODO: testdisk*  

## File Recovery

*TODO: photorec*

*TODO: foremost*

> [ArchWiki - File Recovery](https://wiki.archlinux.org/title/File_recovery#List_of_utilities)  
> [Wikipedia - Data recovery](https://en.wikipedia.org/wiki/Data_recovery#File_Recovery)  
