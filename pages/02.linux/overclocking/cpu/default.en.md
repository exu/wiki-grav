---
title: CPU
visible: true
---

[toc]

## Overclocking

_TODO_ I have not yet looked for system-tools for overclocking

## Testing

> [More Testing Tools can be found on the ArchWiki](https://wiki.archlinux.org/title/Stress_testing)

### Prime95/Mprime

`$ mprime`  
Select "No" when asked to join the distributed computing project  
`16` for torture testing  
Recommended test: `2`

This application includes hardware error checking. Output to the CLI as well as the logfile.  
Check the file `results.txt`

### ffmpeg video encoding

This command encodes random noise with x265 and discards the resulting video  
`$ ffmpeg -y -f rawvideo -video_size 1920x1080 -pixel_format yuv420p -framerate 60 -i /dev/urandom -c:v libx265 -preset placebo -f matroska /dev/null`

> [ArchWiki Source](https://wiki.archlinux.org/title/Stress_testing?useskinversion=1#Video_encoding)

### Stress

Stress is capable of testing CPU, memory, I/O and disks  
Use `$ stress -c (threads)` to test the CPU
