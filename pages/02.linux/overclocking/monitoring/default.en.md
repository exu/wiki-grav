---
title: Monitoring
visible: true
---

[toc]

## Monitoring

### Sensors

The `lm_sensors` package shows temperatures, fan pwm and other sensors for your CPU, GPU and motherboard.  
Run `$ sensors` to get the output.

#### Support for motherboard ITE LPC chips

Support for this type of chip does not come built in to `lm_sensors`.  
In the AUR the package `it87-dkms-git` provides a kernel module with support for a variety of ITE chips. It pulls from [this](https://github.com/frankcrawford/it87) git repo. You can find a list of supported chips there. See [this issue on lm_sensors git repo](https://github.com/lm-sensors/lm-sensors/issues/134) for background info.

The kernel driver can be automatically loaded on boot by putting `it87` into `/etc/modules-load.d/(filename).conf`  
The option `acpi_enforce_resources=lax` also needs to be added to `GRUB_CMDLINE_LINUX_DEFAULT` in `/etc/default/grub` or your bootloader equivalent.

### CoreFreq

[CoreFreq](https://github.com/cyring/CoreFreq) can display a lot of information about the CPU and the memory controller.

To run, the systemd service `corefreqd` needs to be enabled.  
CoreFreq also depends on a kernel driver. Simply put `corefreqk` into `/etc/modules-load.d/(filename).conf` to load it automatically on boot.

Access the TUI using `$ corefreq-cli`

A few interesting views:  
`Shift + C` shows per thread frequency, voltage and power, as well as overall power and temperature.  
`Shift + M` shows the memory timings, frequency and DIMM layout.

### Zenmonitor

[Zenmonitor](https://github.com/ocerman/zenmonitor) is, as the name suggests, monitoring software specifically for AMD Zen CPUs.

### CoreCtrl

[CoreCtrl](https://gitlab.com/corectrl/corectrl)

| Hardware | Supported |
| -------- | --------- |
| AMD CPU  | Minimal   |
| AMD GPU  | Yes       |

CoreCtrl displays a range of information for AMD GPUs.

Make sure to set `amdgpu.ppfeaturemask=0xffffffff` as kernel option.

### Amdgpu Top

| Hardware | Supported |
| -------- | --------- |
| AMD GPU  | Yes       |

Amdgpu Top shows various statistics on GPU utilization (GFX, compute, decode, etc.) and a lot of GPU sensor information.  
The tool is available as TUI by running `amdgpu_top` in a terminal or as full GUI application with `amdgpu_top --gui`

### Error monitoring

Some applications have hardware error reporting built-in.

#### Kernel log

For others, try checking the kernel log.

[shroot]

```
journalctl -k --grep=mce
```

[/shroot]

#### Rasdaemon

You can also install `aur/rasdaemon` and enable its two services.

[shroot]

```
systemctl enable --now ras-mc-ctl.service
systemctl enable --now rasdaemon.service
```

[/shroot]

Show historic errors

[shroot]

```
ras-mc-ctl --summary
```

[/shroot]

Show memory errors during the current session

[shroot]

```
ras-mc-ctl --error-count
```

[/shroot]
