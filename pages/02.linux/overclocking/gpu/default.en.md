---
title: GPU
visible: true
---

[toc]

## Overclocking

### AMD

#### CoreCtrl

[CoreCtrl](/linux/overclocking/monitoring#corectrl) allows the manipulation of GPU frequency, voltages, power and the fancurve.

_Note: CoreCtrl worked for me with an RX 480, but not with an RX 6600 XT_

#### LACT

[LACT](https://github.com/ilya-zlobintsev/LACT)

## Testing

> [More Testing Tools can be found on the ArchWiki](https://wiki.archlinux.org/title/Stress_testing)

### Unigine Benchmarks

- unigine-heaven
- unigine-valley
- unigine-tropics
- unigine-superposition
- unigine-sanctuary
