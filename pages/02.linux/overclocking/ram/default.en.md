---
title: RAM
visible: true
---

[toc]

## Overclocking

_I'm unaware of any platform supporting online-editing of RAM timings_

## Testing

> [More Testing Tools can be found on the ArchWiki](https://wiki.archlinux.org/title/Stress_testing?useskinversion=1)

#### Stressapptest

**NOTE**: Produces heavy load on the CPU as well. A stable CPU OC before running this is recommended.

```sh
stressapptest -M (RAM MiB) -s (time in s) -m (CPU threads)
```
