---
title: Kopia
visible: true
---

[toc]

## Installation

### Debian

```sh
curl -s https://kopia.io/signing-key | sudo apt-key add -
echo "deb http://packages.kopia.io/apt/ stable main" | sudo tee /etc/apt/sources.list.d/kopia.list
sudo apt update
sudo apt install kopia
```

## Connect Repository

To create a new repo, replace "connect" with "create"

### B2

```sh
kopia repository connect b2 \
    --bucket=(bucket name) \
    --key-id=(api key id) \
    --key=(api key)
```

> [Official Documentation](https://kopia.io/docs/reference/command-line/common/repository-connect-b2/)

## Policy

Get global policy  
`# kopia policy get --global`

Change global retention  
Options are `latest, hourly, daily, weekly, monthly, annual`  
`# kopia policy set --keep-(option) (number)`

Change compression  
`# kopiy policy set --compression zstd-best-compression --global`

## Snapshots

`# kopia snapshot create (path)`

`# kopia snapshot list (path)`

> [Check the "Getting Started" Page for more options](https://kopia.io/docs/getting-started/)
