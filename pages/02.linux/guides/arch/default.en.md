---
title: Arch
visible: true
---

[toc]

Last modified: 2024-06-01

## Keyboard layout

[shroot]

```
loadkeys de_CH-latin1
```

[/shroot]

## Check UEFI mode

If the following command works, the system is booted in EFI.

[shroot]

```
ls /sys/firmware/efi/efivars
```

[/shroot]

## Verify internet connection

[shroot]

```
ping wiki.exu.li
```

[/shroot]

## Update system clock

[shroot]

```
timedatectl set-ntp true
```

[/shroot]

## Creating partitions

[shroot]

```
cfdisk
```

[/shroot]

Use `EFI System` for EFI partition  
Use `Linux filesystem` for other partitions

## (LUKS) Create encrypted partition

_Note: Do not put your /efi partition on an encrypted partition!_

Create encrypted Partition with a label. This label will later be used to identified the bootdevice as a simpler alternative to UUIDs.

[shroot]

```
cryptsetup luksFormat --label=(label)disk /dev/(partition)
```

[/shroot]

Open the partition and specify a name

[shroot]

```
cryptsetup open /dev/(partition) (name)mapper
```

[/shroot]

Check if this worked with `ls /dev/mapper/`  
The name should show up there

## Format partitions

### EFI or BIOS partition

Fat 32:

[shroot]

```
mkfs.fat -F32 /dev/(partition)
```

[/shroot]

### Root and other partitions

**Ext4**

[shroot]

```
mkfs.ext4 /dev/(partition)
```

[/shroot]

**Btrfs**

[shroot]

```
mkfs.btrfs /dev/(partition)
```

[/shroot]

**Bcachefs**

[shroot]

```
bcachefs format /dev/(partition)
```

[/shroot]

## Mounting partitions

Generally partitions have to be mounted where you will later use them in your system.  
BTRFS with its subvolumes is a special case, see the additional chapter below.  
Be careful in choosing you EFI mountpoint when using full disk encryption.  
Using `/efi` will lead to much longer boot times in GRUB and be completely unsupported in other bootloaders.  
See [this feature comparison](https://wiki.archlinux.org/title/Arch_boot_process#Boot_loader) for details.  
For `/efi` size really doesn't matter much and can be 1 or 2 megabytes. 16MB is probably a good size.  
If you're using `/boot`, the kernel and initramfs will also be stored on this partition among others. Recommended sizes range from 256MB to 512MB

```
Root: /mnt
EFI: /mnt/efi or /mnt/boot
Home: /mnt/home
etc...
```

### (BTRFS) Btrfs preparation of subvolumes and mounting

Mount root partition

[shroot]

```
mount /dev/(partition) /mnt
```

[/shroot]

Root subvolume

[shroot]

```
btrfs subv create /mnt/@
```

[/shroot]

Home subvolume

[shroot]

```
btrfs subv create /mnt/@home
```

[/shroot]

Snapshots subvolume for snapper

[shroot]

```
btrfs subv create /mnt/@snapshots
```

[/shroot]

Var subvolume

[shroot]

```
btrfs subv create /mnt/@var_log
```

[/shroot]

[shroot]

```
umount /mnt
```

[/shroot]

Mount root  
`mount -o noatime,compress-force=zstd,subvol=@ /dev/(partition) /mnt`

With /efi  
`mkdir -p /mnt/{efi,home,.snapshots,var/log}`  
With /boot  
`mkdir -p /mnt/{boot,home,.snapshots,var/log}`

Mount home  
`mount -o noatime,compress-force=zstd,subvol=@home /dev/(partition) /mnt/home`

Mount snapshots for snapper  
`mount -o noatime,compress-force=zstd,subvol=@snapshots /dev/(partition) /mnt/.snapshots`

Mount var/log
`mount -o noatime,compress-force=zstd,subvol=@var_log /dev/(partition) /mnt/var/log`

**Don't forget mounting other partitions!!**

## Essential packages

Install a few essential packages using `pacstrap`.  
Additional packages might also be necessary, see the list below.

[shroot]

```
pacstrap /mnt base base-devel linux linux-firmware linux-headers vim git openssh networkmanager dialog
```

[/shroot]

### Other packages

| Package                   | Category      | Component    |
| ------------------------- | ------------- | ------------ |
| `amd-ucode`               | CPU microcode |
| `intel-ucode`             | CPU microcode |
| `dosfstools`              | Filesystems   | Fat32        |
| `mtools`                  | Filesystems   | Fat32        |
| `e2fsprogs`               | Filesystems   | Ext4         |
| `btrfs-progs`             | Filesystems   | Btrfs        |
| `compsize`                | Filesystems   | Btrfs        |
| `wpa_supplicant`          | Wifi          |
| `snapper`                 | System        | Snapshots    |
| `ca-certificates`         | System        | Certificates |
| `ca-certificates-mozilla` | System        | Certificates |
| `zram-generator`          | System        | Zram         |
| `cups`                    | Printing      | General      |
| `hplip`                   | Printing      | HP           |
| `xdg-utils`               | System        | Defaults     |
| `xdg-user-dirs`           | System        | Defaults     |
| `inetutils`               | Tools         | Networking   |

## Generate fstab

[shroot]

```
genfstab -U /mnt >> /mnt/etc/fstab
```

[/shroot]

**Make sure the fstab file has everything included**

## Chroot into the system

[shroot]

```
arch-chroot /mnt
```

[/shroot]

## Set timezone

`ln -sf /usr/share/zoneinfo/Europe/Zurich /etc/localtime`

## Set hardware clock

`hwclock --systohc`

## Set locale

`vim /etc/locale.gen`  
Uncomment the locales that should be generated.  
Make sure to use a UTF-8 entry.

`locale-gen`

`echo "LANG=de_CH.UTF-8" > /etc/locale.conf`

## Set keymap permanently

`echo "KEYMAP=de_CH-latin1" > /etc/vconsole.conf`

## Set hostname

`echo "(hostname)" > /etc/hostname`

Edit `/etc/hosts`

```
127.0.0.1   localhost (hostname)
::1         localhost
```

## Change root password

[shroot]

```
passwd
```

[/shroot]

## Configure zram

Create the file `/etc/systemd/zram-generator.conf`

```systemd
[zram0]
zram-size = ram / 2
compression-algorithm = zstd
```

## Bootloader installation

### rEFInd

[shroot]

```
pacman -S refind
```

[/shroot]

Use the rEFInd installation script. In most cases no specific configuration is necessary

[shroot]

```
refind-install
```

[/shroot]

**Manual editing of the generated configuration file is necessary when installing rEFInd from the Arch boot ISO**

#### Autodetection

`/boot/refind_linux.conf`

With LUKS:

```
"Boot with standard options" "rw loglevel=3 quiet cryptdevice=LABEL=<label>disk:<name>mapper root=/dev/mapper/<name>mapper rootflags=subvol=@ zswap.enabled=0"
```

With unencrypted disks, only `rw` and `root=` are required.  
Make sure to include additional kernel options like `zswap.enabled=0` if required.

```
"Boot with standard options"  "rw root=UUID=XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX"
"Boot to single-user mode"    "rw root=UUID=XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX single"
"Boot with minimal options"   "ro root=PARTUUID=XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX"
```

#### Manual boot stanza

Assuming you use a `/boot` partition containing the `EFI` directory and linux kernels.  
`refind-nord-theme` is required for the icon configured below. Install it from the AUR or change the icon to `/EFI/refind/icons/os_arch.png`.

`/boot/EFI/refind/refind.conf`

```
menuentry "Arch Linux" {
    icon     /EFI/refind/themes/nord/icons/os_arch.png
    loader   /vmlinuz-linux
    initrd   /initramfs-linux.img
    options  "rw loglevel=3 quiet cryptdevice=LABEL=(label):(name) root=/dev/mapper/(name) rootflags=subvol=@ zswap.enabled=0"
    submenuentry "Boot using fallback initramfs" {
        initrd /initramfs-linux-fallback.img
    }
    submenuentry "Boot to terminal" {
        add_options "systemd.unit=multi-user.target"
    }
}
```

Edit `/boot/EFI/refind/themes/nord/theme.conf` to show keyboard options by removing `hints` from `hideui`

```
...
hideui singleuser,badges
...
```

### GRUB

[shroot]

```
pacman -S grub efibootmgr
```

[/shroot]

Make sure the `/boot` or `/efi` partition is mounted

[shroot]

```
grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB
```

[/shroot]

Generate the grub config file

[shroot]

```
grub-mkconfig -o /boot/grub/grub.cfg
```

[/shroot]

### (LUKS) mkinitcpio with LUKS

Some additional settings are necessary in `/etc/mkinitcpio.conf` in order to enable booting from LUKS-encrypted disks.

Edit the `HOOKS` section in `/etc/mkinitcpio.conf` by adding `encrypt` after `block` and before `filesystems`

Also ensure `keyboard keymap` are present before `encrypt` in order to allow the loading of the default keyboard language from `/etc/vconsole.conf` to enter the decryption password.

Regenerate all initramfs presets

[shroot]

```
mkinitcpio -P
```

[/shroot]

## Networking

`systemctl enable NetworkManager`

## (CUPS) Printing

`systemctl enable cups`

## Add user

`useradd -mG wheel (user)`

Set password  
`passwd (user)`

### Enable sudo

`visudo`  
Uncomment `%wheel ALL=(ALL) ALL`

## Finishing installation

`exit`  
`poweroff`  
Remove the installation cd

## Post-reboot

### Zram

Create the zram device

[shroot]

```
systemctl daemon-reload
systemctl start systemd-zram-setup@zram[X].service
```

[/shroot]

## (Snapper) Setup

`# umount /.snapshots`

`# rm -r /.snapshots`

Create snapper config  
`# snapper -c root create-config /`

Delete unneeded volume  
`# btrfs subv del /.snapshots/`

`# mkdir /.snapshots`

Mount snapshots volume  
`# mount -a`

`# chmod 750 /.snapshots`

`# vim /etc/snapper/configs/root`

Change these things:  
`ALLOW_USERS="(user)"`

```
TIMELINE_LIMIT_HOURLY="5"
TIMELINE_LIMIT_DAILY="7"
TIMELINE_LIMIT_WEEKLY="4"
TIMELINE_LIMIT_MONTHLY="0"
TIMELINE_LIMIT_YEARLY="0"
```

Enable snapper  
`# systemctl enable --now snapper-timeline.timer`  
`# systemctl enable --now snapper-cleanup.timer`

Allow user to access snapshots  
`# chmod a+rx /.snapshots`  
`# chown :(user) /.snapshots`

## Install AUR helper

### paru

[shuser]

```
cd $(mktemp -d)
git clone https://aur.archlinux.org/paru-bin.git
cd paru-bin
makepkg -si
```

[/shuser]

## Automatic snapshots on package changes

[shroot]

```
pacman -S snap-pac
```

[/shroot]

## (rEFInd) Show snapshots in bootmenu

This requires a manual boot entry definition in `/boot/EFI/refind/refind.conf` as shown in [Manual boot stanza](#manual-boot-stanza)

`paru -S refind-btrfs`

In `/etc/refind-btrfs.conf`, embed the btrfs logo.

```
...
[boot-stanza-generation.icon]
mode = "embed_btrfs_logo"
...
```

Use the inverted logo for darker themes such as Nord.

```
...
[boot-stanza-generation.icon.btrfs-logo]
variant = "inverted"
...
```

`systemctl enable --now refind-btrfs.service`

**NOTE:** this doesn't work yet, as the UUID naming scheme is not supported by refind-btrfs.  
See [issue 46](https://github.com/Venom1991/refind-btrfs/issues/46) for details.
