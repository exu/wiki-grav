---
title: "Debian create new template"
visible: true
---

[toc]

This guide focuses on creating a new template for Debian that can be used with XCP-ng.

## VM Creation

Before even starting the installation, a new VM has to be created.  
I choose the `Other install media` template and set the Distro name prefixed with "CR".  
The usual sizing is 1 vCPU and 1 GiB RAM.  
Add a network interface in the most commonly used network.  
Add a disk, a size of 10 GiB is a good starting point.  
Finally in the advanced settings, change the boot firmware from `bios` to `uefi`

## Installation

Most of the installation can be done as usual, though some parts must be observed.  
The root user will get a password. We also create a new non-root user.  
For partitioning, choose the manual method.  
The following partitions should be created, all of them as primary partitions.

- Type **EFI** _128M_
- Type **Swap** _1G_
- Type **EXT4** _remaining space_

![Partitions overview](partitioning.webp)

Accept the created partitioning

> The reason for this layout is so Cloud-init can automatically grow the root partition when the underlying virtual disk is expanded

Reboot after the installation completes.

## Cloud-init

_Note: Cloud-init packages are included in my base Ansible configuration_  
Install Cloud-init to use its configurations.  
On debian the required packages are `cloud-init` and `cloud-initramfs-growroot`

> More information can be found on the [Cloud-init page](/linux/cloud-init).

## Final Configuration

### Ansible

To guarantee smooth operation with terraform, the Ansible playbooks `server_base` and `xe_guest_utilities` should be executed now.

### Networking

Edit `/etc/network/interfaces` and remove the network interface defined there.  
Cloud-init will create a new definition under `/etc/network/interfaces.d`

### ISO-file

Unmount and remove the installation ISO-file.

## Conversion

Poweroff the VM and go to its `Advanced` tab.  
![XCP-ng Advanced tab](xcp-ng-advanced-tab.webp)

Before clicking `Convert to template`, creating a clone is recommended.
