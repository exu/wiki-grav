---
title: "Debian Raspberry Pi 3/4"
visible: true
---

[toc]

## Image

The image can be downloaded from the following link.  
[https://raspi.debian.net/](https://raspi.debian.net/)

By default the only user account available is root with an empty password.

## Setup in tty

1. Install `keyboard-configuration` and `console-setup` to select the keyboard layout
2. Create a [new user](/linux/users-groups)
   - Don't forget to add it to the "sudo" group
3. Check what IP address the device has
4. Install `sudo`

## Setup in ssh

1. Change the hostname to something sensible
   - Change in /etc/hostname
   - Add the line `127.0.1.1   (hostname)` in /etc/hosts
