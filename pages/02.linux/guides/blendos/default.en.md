---
title: blendOS
visible: false
---

[toc]

Last modified: 2024-06-01

# Alpha v4 Installation Requirements

- ArchLinux
- GRUB bootloader
- mkinitcpio
- No FDE

# Installation

Add this content to `/etc/pacman.conf`

```conf
[breakfast]
SigLevel = Never
Server = https://pkg-repo.blendos.co
```

Create the file `/system.yaml`

```yaml
repo: "https://pkg-repo.blendos.co/"

impl: "https://github.com/blend-os/tracks/raw/main"

# all tracks: https://github.com/blend-os/tracks
track: "blendos-base"

packages:
  - "firefox"
#    - 'nvidia-dkms' if you're using an NVIDIA GPU
```

[shroot]

```
pacman -Sy akshara
```

[/shroot]

Add `akshara` after `base udev` in `/etc/mkinitcpio.conf` under `HOOKS`

[shroot]

```
mkinitcpio -P
akshara update
```

[/shroot]

Reboot the system
