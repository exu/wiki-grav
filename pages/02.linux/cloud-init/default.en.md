---
title: Cloud-Init
visible: true
---

[toc]

## VM Preparation

The VM template needs a few cloud-init tools installed before we can use it with cloud-init configs

### Debian

[shroot]

```sh
apt install cloud-init cloud-initramfs-growroot
```

[/shroot]

## Cloud-init

Cloud-init has a config file in `/etc/cloud/cloud.cfg`

### Growing partitions

With growroot installed in the initramfs, cloud-init will expand partitions and filesystems to fit the root partition given.

### XCP-ng: Set Hostname

This cloud config snippet sets the machine hostname to the VM name.

```
#cloud-config
hostname: {name}
```

## Network config

The network config is a special config file with cloud-init that focuses on specifying network settings.

### Set Static IP

The snippet below sets a static ip address for the specified interface.  
Its syntax mainly follows the `/etc/network/interfaces` syntax.

```
network:
  version: 1
  config:
  - type: physical
    name: [INTERFACE]
    subnets:
      - type: static
        address: [ADDRESS]
        gateway: [ADDRESS]
        dns_nameservers: [ADDRESS]
```
