---
title: "MariaDB Replication"
visible: true
---

[toc]

## Master Slave Setup

### Master Configuration

The MariaDB Server has to be accessible from outside. For Debian, one has to comment `bind-address=127.0.0.1` in the file `/etc/mysql/mariadb.conf.d/50-server.cnf`.  
If you have any firewall enabled, make sure to allow port 3306/TCP.

Add this segment at the end of `/etc/mysql/my.cnf`

```ini
[mariadb]
log-bin
server_id=1
log-basename=master
binlog-format=mixed
```

**Restart mariadb** now

Create a replication user

```sql
CREATE USER 'replication'@'%' IDENTIFIED BY '<password>';
GRANT REPLICATION SLAVE ON *.* TO 'replication'@'%';
```

Next we have to get the data necessary so the slave knows where to start replicating.

```sql
FLUSH TABLES WITH READ LOCK;
SHOW MASTER STATUS;
```

**Do not close this session, keep it running until you have made the backup from the next step**  
`# mysqldump -u root -p (db name) > db_name.sql`

You can unlock the database again.  
`UNLOCK TABLES;`

### Slave Configuration

Edit your `/etc/mysql/my.cnf` file  
Make sure to choose different IDs for every host

```ini
[mysqld]
server-id = 2
```

Create the database and restore the sql dumps made earlier.  
`# mysql -u root -p (db name) < db_name.sql`

Set the database master now

```sql
CHANGE MASTER TO
  MASTER_HOST='<domain>',
  MASTER_USER='replication',
  MASTER_PASSWORD='<password>',
  MASTER_PORT=3306,
  MASTER_LOG_FILE='<master log file>',
  MASTER_LOG_POS=<master log position>,
  MASTER_CONNECT_RETRY=10,
  MASTER_USE_GTID = slave_pos;
```

Start slave now  
`START SLAVE;`  
And check the status  
`SHOW SLAVE STATUS \G`

If both of the following options say yes, everything is working as intended

```
Slave_IO_Running: Yes
Slave_SQL_Running: Yes
```
