---
title: Navidrome
visible: true
---

[toc]

## Installation

```sh
podman run \
    --name navidrome \
    -p 4533:4533 \
    -v /path/to/music:/music:ro \
    -v /path/to/data:/data \
    -e ND_ENABLETRANSCODINGCONFIG=true \
    -e ND_SCANSCHEDULE=1h \
    -e ND_COVERJPEGQUALITY=90 \
    -e ND_DEFAULTTHEME="Electric Purple" \
    -d docker.io/deluan/navidrome:latest
```
