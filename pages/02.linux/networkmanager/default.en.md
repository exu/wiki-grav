---
title: "NetworkManager"
visible: true
---

[toc]

## nmcli

First, find the connection you want to edit  
The full list can be shown using this command  
[shuser]

```sh
nmcli connection
(out)NAME           UUID                                  TYPE       DEVICE
(out)enp5s0         889b187d-2a9f-4e4d-938c-c88b85fb5756  ethernet   enp5s0
(out)lo             f61a36ee-b958-4940-8145-b61e92dc59d2  loopback   lo
```

[/shuser]

Enter the connection editor for an interface

[shuser]

```sh
nmcli connection edit 'enp5s0'
```

[/shuser]

Print IPv4 settings

```
nmcli> print ipv4
```

Set IPv4 method to "disabled", "auto", "manual", or "link-local"

```
nmcli> ipv4.method <method>
```

```
auto: DHCP
manual: manual configuration
```

Set IPv4 address(es)

```
nmcli> set ipv4.addresses <address/cidr>, ...
```

Set IPv4 gateway

```
nmcli> print ipv4.gateway <address>
```

Set IPv4 DNS

```
nmcli> print ipv4.dns <address>
```

Save the setting persistently

```
nmcli> save
```

Exit configuration interface

```
nmcli> quit
```

> [NetworkManager - Edit a connection](https://wiki.archlinux.org/title/NetworkManager#Edit_a_connection)
