---
title: "Arr Stack"
visible: false
---

[toc]

## Folder Layout

The recommended folder layout keeps the various pieces necessary within one folder.  
This top folder will be shared with Sonarr or Radarr, while Jellyfin and Transmission respectively only need a partial view.

> [Setup for Docker](https://trash-guides.info/Hardlinks/How-to-setup-for/Docker/)

## Sonarr

[shroot]

```
podman run -d \
    --name sonarr \
    -p 8989:8989 \
    -v /mnt/sonarr:/config \
    -v /mnt/arrdata:/mnt/arrdata \
    cr.hotio.dev/hotio/sonarr:latest
```

[/shroot]

Configure an application in Prowlarr that points to Sonarr  
![Prowlarr application configuration dialogue for Sonarr](prowlarr-app-sonarr.png)

Now configure the download client in Sonarr  
![Sonarr configuration for download client](sonarr-download-client.png)

### Anime

Some special profiles will be created to get better anime releases

> [Release Profile RegEx (Anime)](https://trash-guides.info/Sonarr/Sonarr-Release-Profile-RegEx-Anime/)

## Radarr

[shroot]

```
podman run -d \
    --name radarr \
    -p 7878:7878 \
    -v /mnt/radarr:/config \
    -v /mnt/arrdata:/mnt/arrdata \
    cr.hotio.dev/hotio/radarr:latest
```

[/shroot]

Same as with Sonarr, configure Radarr as an application in Prowlarr  
![Prowlarr application configuration dialogue for Radarr](prowlarr-app-radarr.webp)

As with Sonarr, configure Transmission as available download client  
![Radarr download client configuration dialogue](radarr-download-client.png)
