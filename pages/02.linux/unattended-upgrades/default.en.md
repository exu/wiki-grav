---
title: Unattended Upgrades
visible: true
---

[toc]

## Installation

`# apt install unattended-upgrades`

## Configuration

**NOTE: This configuration is tailored to my personal preferences. Feel free to do something else if you don't want what I'm doing**

### Enable automatic reboots

If necessary, the server will automatically reboot.  
An example would be kernel updates.

Edit `/etc/apt/apt.conf.d/50unattended-upgrades`

```
...
Unattended-Upgrade::Automatic-Reboot "true";
...
```

### Repo update time

Create an override file for `apt-daily.timer` using this command  
`$ sudo systemctl edit apt-daily.timer`

Add these lines between the two comments

```systemd
[Timer]
OnCalendar=*-*-* 2:00
RandomizedDelaySec=0
```

### Host upgrade time

Create an override file for `apt-daily-upgrade.timer` using this command  
`$ sudo systemctl edit apt-daily-upgrade.timer`

Add these lines between the two comments

```systemd
[Timer]
OnCalendar=*-*-* 4:00
RandomizedDelaySec=0
```
