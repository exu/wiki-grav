---
title: Unmanic
visible: false
---

[toc]

## Networking

```
8888: Port for Webinterface
```

## Installation

```
podman run -itd \
    --name unmanic  \
    -e PUID=0 \
    -e PGID=0 \
    -p 8888:8888 \
    -v /mnt/unmanic/config:/config \
    -v /mnt/unmanic/medialib:/medialib \
    -v /mnt/unmanic/cache:/tmp/unmanic \
    docker.io/josh5/unmanic:latest
```

If the connection will be established through a webserver running on the same machine, use `-p 127.0.0.1:8888:8888` instead to only allow connections from the local host.

## Authentication

> [nginx basic auth](https://docs.nginx.com/nginx/admin-guide/security-controls/configuring-http-basic-authentication/)

Unmanic does not have any authentication built in. However, it does support basic authentication for remote servers.  
To use basic auth however, a webserver has to be configured through which Unmanic will be accessed.

Nginx will be used as the webserver, while apache2-utils is necessary to create the password file.

```sh
sudo apt install nginx apache2-utils
```

Create a new password file

```sh
sudo htpasswd -c /etc/apache2/.htpasswd [USER]
```

Additional users can be added by omitting the `-c` switch

```sh
sudo htpasswd /etc/apache2/.htpasswd [USER]
```

Nginx configuration file

```nginx
server {
    server_name unmanic.ovh1app1.x9w.ch;

    # Security / XSS Mitigation Headers
    add_header X-Frame-Options "SAMEORIGIN";
    add_header X-XSS-Protection "1; mode=block";
    add_header X-Content-Type-Options "nosniff";
    #add_header Strict-Transport-Security "max-age=31536000; includeSubDomains";

    client_max_body_size 40G;

    location / {
        # Proxy main traffic
        proxy_pass http://127.0.0.1:8888;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-Protocol $scheme;
        proxy_set_header X-Forwarded-Host $http_host;

        # Proxy WebSocket connection
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "Upgrade";

        # Basic Authentication
        auth_basic "Unmanic";
        auth_basic_user_file /etc/apache2/.htpasswd;
    }

    listen *:80;

    #listen *:443 ssl http2;
    #ssl_certificate_key /etc/certs/unmanic.ovh1app1.x9w.ch/cert.key;
    #ssl_certificate /etc/certs/unmanic.ovh1app1.x9w.ch/cert.crt;
}

#server {
#    if ($host = unmanic.ovh1app1.x9w.ch) {
#        return 301 https://$host$request_uri;
#    }

#    listen 80;
#    listen [::]:80;
#    server_name unmanic.ovh1app1.x9w.ch;
#    return 404;
#}

```
