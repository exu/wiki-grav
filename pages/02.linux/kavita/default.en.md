---
title: Kavita
visible: true
---

[toc]

## Create directories

```sh
mkdir -p /var/kavita/{config,content}
mkdir -p /var/kavita/content/{manga,books,tech}
```

## Run Kavita

```sh
# podman run --name kavita -p 5000:5000 \
    -v /var/kavita/content:/content \
    -v /var/kavita/config:/kavita/config \
    --restart unless-stopped \
    -d docker.io/kizaing/kavita:latest
```

## Nginx Config

```nginx
server {
    server_name kavita.exu.li;

    # Security / XSS Mitigation Headers
    add_header X-Frame-Options "SAMEORIGIN";
    add_header X-XSS-Protection "1; mode=block";
    add_header X-Content-Type-Options "nosniff";
    add_header Strict-Transport-Security "max-age=31536000; includeSubDomains";

    location / {
        # Proxy main traffic
        proxy_pass http://172.16.53.100:5000;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-Protocol $scheme;
        proxy_set_header X-Forwarded-Host $http_host;
    }

    listen *:443 ssl http2; #set ipv6 address
    ssl_certificate_key /etc/acme-sh/kavita.exu.li/key.pem;
    ssl_certificate /etc/acme-sh/kavita.exu.li/cert.pem;
}

server {
    if ($host = kavita.exu.li) {
        return 301 https://$host$request_uri;
    }

    listen *:80; #set ipv6 address
    server_name kavita.exu.li;
    return 404;
}
```

## Systemd Service

> See [Podman](/linux/podman#generate-systemd-service) to generate a service file.
