---
title: Step-CA
visible: true
---

[toc]

## Server

```sh
podman run -d --name step-ca \
    -v step:/home/step \
    -p 9000:9000 \
    -e "DOCKER_STEPCA_INIT_NAME=Demiurge" \
    -e "DOCKER_STEPCA_INIT_DNS_NAMES=(hostname),(hostname2)" \
    docker.io/smallstep/step-ca
```

Get the root ca fingerprint  
`# podman run -v step:/home/step smallstep/step-ca step certificate fingerprint certs/root_ca.crt`

To view your ca password, run this command  
`# podman run -v step:/home/step smallstep/step-ca cat secrets/password`

### ACME Server

Enable ACME. Restart the server afterwards.  
`$ step ca provisioner add acme --type ACME`

## Client

Initialize the step-cli client  
`step-cli ca bootstrap --ca-url https://(domain/ip):9000 --fingerprint (root_ca fingerprint)`

## Create Certificates

> [Official documentation](https://smallstep.com/docs/step-cli/basic-crypto-operations)

Enter the container  
`# podman exec -it step-ca bash`

### Client Certificate

```sh
step certificate create (cert name) client-certs/(cert name).crt client-certs/(cert name).key \
    --profile leaf --not-after=8760h \
    --ca certs/intermediate_ca.crt \
    --ca-key secrets/intermediate_ca_key \
    --bundle
```

Add SANs with the `--san=`-flag. Add multiple flags for multiple SANs.

### ACME

Point your ACME client to `https://(domain/ip):9000/acme/(provisioner-name)/directory`

## Device Truststore

### Arch Linux

> [Archwiki Article on TLS](https://wiki.archlinux.org/title/Transport_Layer_Security#Add_a_certificate_to_a_trust_store)

Add new trust anchor  
`# trust anchor (root ca.crt)`  
List trust anchors  
`$ trust list`
