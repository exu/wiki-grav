---
title: "Nextcloud OIDC"
visible: true
media_order: "authentik-create-provider.webp, authentik-create-application.webp, nextcloud-openid-connect.webp, authentik-custom-property-mapping.webp, authentik-oauth-scope.webp"
---

[toc]

This guide will be using OIDC with the [user_oidc](https://github.com/nextcloud/user_oidc) plugin.

## Authentik

### Create provider

First, a new provider needs to be created. The setting can be found under `Applications > Providers`, click `Create`

In the first screen, select `OAuth2/OpenID Provider` and click `Next`

- _Authorization flow_: default-provider-authorization-implicit-consent (Choosing explicit consent instead means the user has to approve every login)
- _Client type_: Confidential
- _Client ID_: The auto generated value is fine, copy it for use later.
- _Client Secret_: **WARNING** user_oidc currently only supports values up to 64 characters in length. Make sure to trim the value below that, or generate a new secret with less characters. `openssl rand -base64 40` [Issue on user_oidc GitHub](https://github.com/nextcloud/user_oidc/issues/405)
- _Redirect URIs/Origins_: `https:\/\/nextcloud\.example\.com.*`
- _Signing Key_: Set this to a valid TLS Certificate

![Screenshot of the provider creation page with settings filled in](authentik-create-provider.webp)

### Create application

Now it is time to create the application.  
Go to `Applications > Applications` and click `Create`  
Set a name and choose the previously created provider under the `Provider` setting.  
Other settings can be left at their defaults.

![Screenshot of application creation dialog](authentik-create-application.webp)

Now, go back to the `Providers` screen and click on the previously created provider.  
Copy the value from `OpenID Configuration URL`, it should be something like `https://{AUTHENTIK URL}/application/o/{PROVIDER NAME}/.well-known/openid-configuration`

## Nextcloud

Log in as administrator, go to `Apps` and search for `OpenID Connect user backend`  
Click `Download and Enable` to install the app.

Next, open the administration settings in Nextcloud and go to `OpenID Connect`.  
Click on the `+` below `Registered Providers`

- _Identifier_: Authentik (This value is shown to the users when they try to log in)
- _Client ID_: (value copied from Authentik)
- _Client Secret_: (value copied from Authentik)
- _Discovery endpoint_: (OpenID Configuration URL copied from Authentik, should end with .well-known/openid-configuration)

**Attribute mapping**

- _User ID mapping_: `sub`
- _Display name mapping_: `nickname`

- Uncheck `Use unique user id`, otherwise nextcloud will hash the provided user id mapping together with the provider and use that as identifier. This is unnecessary unless you're using multiple providers with non-unique names.
- Check `Use group provisioning` in order to create and update user groups in Nextcloud from Authentik.

All other settings here should be left at their default.

![OpenID Connect Plugin provider settings](nextcloud-openid-connect.webp)

If you are running the Authentik in the same local network as Nextcloud and use internal addresses, you also need to add the setting `'allow_local_remote_servers' => true,` to your `config.php` file.  
Otherwise Nextcloud rejects the connection.

## Setting user quotas in Authentik

Using custom attributes, property mappings and scope mappings it is possible to set the desired storage quota for users.

### Assign custom attribute

Go to `Directory > Users`, click on a user and select `Edit`.  
In the field `Attributes` custom attributes can be specified in JSON or YAML format.

_Example_:

```yaml
app-nextcloud-quota: 20G
```

### Create propery mapping

Go to `Customisation > Property Mappings` and create a new mapping of the type `Scope Mapping`

The name can be chosen freely, choose something identifiable.  
Scope name will be used in the Nextcloud OpenID Connect config as scope.  
The expression is used to get the previously created custom attribute.

```py
return {
    "quota": request.user.attributes.get("app-nextcloud-quota", "default"),
}
```

![Authentik custom property mapping settings](authentik-custom-property-mapping.webp)

### Expose propery mapping

Click on the previously created provider for Nextcloud and select `Edit`.  
Go to `Advanced protocol settings > Scopes` and `CTRL + Click` the newly created Nextcloud quota mapping.

![Selected scope mappings](authentik-oauth-scope.webp)

### Nextcloud config

Nextcloud needs to request access to the scope we just created.  
Simply add `quota` to the space separated list of Scopes in the OpenID Connect provider settings.

Changing the quota attribute, will update the storage quota for the user upon the next login.

## Other links

> [Complete guide to Nextcloud OIDC authentication with Authentik](https://blog.cubieserver.de/2022/complete-guide-to-nextcloud-oidc-authentication-with-authentik/)
