---
title: Authentik
visible: true
---

[toc]

## Podman

### Podman Socket

Podman in version `3.0` comes with the socket already enabled for the root user.

> [https://github.com/containers/podman/blob/main/docs/tutorials/socket_activation.md](https://github.com/containers/podman/blob/main/docs/tutorials/socket_activation.md)

### Network and Pod

[shroot]

```sh
podman network create net_authentik
podman pod create --name pod_authentik --network net_authentik -p 9000:9000 -p 9443:9443
```

[/shroot]

#### Port Mappings

```
9000: Authentik HTTP
9443: Authentik HTTPS
```

### Database

[shroot]

```sh
podman run --name authentik_db \
    -e PGDATA=/var/lib/postgresql/data/pgdata \
    -e POSTGRES_USER=${DB_USER} \
    -e POSTGRES_PASSWORD=[DB_PASS] \
    -e POSTGRES_DB=[DB_NAME] \
    -v /mnt/authentik_db:/var/lib/postgresql/data \
    --pod pod_authentik \
    -d docker.io/postgres:15
```

[/shroot]

### Redis

[shroot]

```sh
podman run --name authentik_redis \
    -v /mnt/authentik_redis:/data \
    --pod pod_authentik \
    -d docker.io/redis:7 \
    redis-server --save 60 1 --loglevel warning
```

[/shroot]

### Application Server

https://goauthentik.io/docs/installation/docker-compose

Generate `PG_PASS` and `AUTHENTIK_SECRET_KEY` using `openssl rand -base64 40`

[shroot]

```sh
podman run --name authentik_server \
    -e PG_PASS={RANDOM PASS} \
    -e AUTHENTIK_SECRET_KEY={RANDOM SECRET} \
    -e AUTHENTIK_REDIS__HOST=authentik_redis \
    -e AUTHENTIK_POSTGRESQL__HOST=authentik_db \
    -e AUTHENTIK_POSTGRESQL__USER={DB USER} \
    -e AUTHENTIK_POSTGRESQL__NAME={DB NAME} \
    -e AUTHENTIK_POSTGRESQL__PASSWORD={DB PASS} \
    # SMTP Host Emails are sent to
    -e AUTHENTIK_EMAIL__HOST={SMTP SERVER} \
    -e AUTHENTIK_EMAIL__PORT=465 \
    # Optionally authenticate (don't add quotation marks to your password)
    -e AUTHENTIK_EMAIL__USERNAME={SMTP USER} \
    -e AUTHENTIK_EMAIL__PASSWORD={SMTP PASS} \
    # Use StartTLS
    -e AUTHENTIK_EMAIL__USE_TLS=false \
    # Use SSL
    -e AUTHENTIK_EMAIL__USE_SSL=true \
    -e AUTHENTIK_EMAIL__TIMEOUT=10 \
    # Email address authentik will send from, should have a correct @domain
    -e AUTHENTIK_EMAIL__FROM={EMAIL} \
    -v /mnt/authentik/media:/media \
    -v /mnt/authentik/templates:/templates \
    -v /mnt/authentik/geoip:/geoip \
    --pod pod_authentik \
    -d ghcr.io/goauthentik/server:2023.2 \
    server
```

[/shroot]

### Application Worker

> NOTE: Docker connection not working with the podman.sock yet

[shroot]

```sh
podman run --name authentik_worker \
    -e PG_PASS={RANDOM PASS} \
    -e AUTHENTIK_SECRET_KEY={RANDOM SECRET} \
    -e AUTHENTIK_REDIS__HOST=authentik_redis \
    -e AUTHENTIK_POSTGRESQL__HOST=authentik_db \
    -e AUTHENTIK_POSTGRESQL__USER={DB USER} \
    -e AUTHENTIK_POSTGRESQL__NAME={DB NAME} \
    -e AUTHENTIK_POSTGRESQL__PASSWORD={DB PASS} \
    # SMTP Host Emails are sent to
    -e AUTHENTIK_EMAIL__HOST={SMTP SERVER} \
    -e AUTHENTIK_EMAIL__PORT=465 \
    # Optionally authenticate (don't add quotation marks to your password)
    -e AUTHENTIK_EMAIL__USERNAME={SMTP USER} \
    -e AUTHENTIK_EMAIL__PASSWORD={SMTP PASS} \
    # Use StartTLS
    -e AUTHENTIK_EMAIL__USE_TLS=false \
    # Use SSL
    -e AUTHENTIK_EMAIL__USE_SSL=true \
    -e AUTHENTIK_EMAIL__TIMEOUT=10 \
    # Email address authentik will send from, should have a correct @domain
    -e AUTHENTIK_EMAIL__FROM={EMAIL} \
    -v /var/run/podman/podman.sock:/var/run/docker.sock \
    -v /mnt/authentik/certs:/certs \
    -v /mnt/authentik/media:/media \
    -v /mnt/authentik/templates:/templates \
    -v /mnt/authentik/geoip:/geoip \
    --pod pod_authentik \
    -d ghcr.io/goauthentik/server:2023.2 \
    worker
```

[/shroot]

### Nginx

```nginx
# Upstream where your authentik server is hosted.
upstream authentik {
    server 172.18.50.10:9443;
    # Improve performance by keeping some connections alive.
    keepalive 10;
}

# Upgrade WebSocket if requested, otherwise use keepalive
map $http_upgrade $connection_upgrade_keepalive {
    default upgrade;
    ''      '';
}

server {
    server_name auth.exu.li;

    # Proxy site
    location / {
        proxy_pass https://authentik;
        proxy_http_version 1.1;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header Host $host;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection $connection_upgrade_keepalive;
    }

    listen 443 ssl http2;
    listen [::]:443 ssl http2;
    ssl_certificate_key /etc/acme-sh/auth.exu.li/key.pem;
    ssl_certificate /etc/acme-sh/auth.exu.li/cert.pem;
}

server {
    if ($host = auth.exu.li) {
        return 301 https://$host$request_uri;
    }

    listen 80;
    listen [::]:80;
    server_name auth.exu.li;
    return 404;
}
```

### Systemd Services

_TODO_

## Setup

After starting all containers, visit the path `https://{SERVER IP}:{PORT}/if/flow/initial-setup/` in your browser.  
The default user is called `akadmin`

## Manual LDAP Outpost

Manual deployment of outposts is necessary, if no integration for kubernetes or docker is provided to authentik.  
Outposts are available as OCI-containers and can be deployed like any other container.

> [https://goauthentik.io/docs/outposts/manual-deploy-docker-compose](https://goauthentik.io/docs/outposts/manual-deploy-docker-compose)

[shroot]

```sh
podman run --name authentik_ldap -p 389:3389 -p 636:6636 \
    -e AUTHENTIK_HOST=https://{AUTHENTIK URL} \
    -e AUTHENTIK_INSECURE=false \
    -e AUTHENTIK_TOKEN={GENERATED TOKEN} \
    -d ghcr.io/goauthentik/ldap:2023.2
```

[/shroot]
