---
title: Jellyfin LDAP
visible: false
---

[toc]

https://goauthentik.io/docs/providers/ldap/generic_setup  
https://goauthentik.io/integrations/services/jellyfin/
