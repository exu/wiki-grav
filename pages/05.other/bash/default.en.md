---
title: Bash
visible: true
---

[toc]

## Returning exit status

`exit 1`

| Code | Meaning |
| ---- | ------- |
| 0    | Success |
| 1    | Error   |

## Check for Arguments given

```sh
if [ $# -eq 0 ]; then
    echo "Please supply one argument"
    $(exit 1); echo "$?"
elif [ $# -ge 2 ]; then
    echo "Please give only one argument"
    $(exit 1); echo "$?"
fi
```

## Multiline output

```sh
cat << EOF
Line 1
Line 2
Line 3
EOF
```

Will output:

```
Line 1
Line 2
Line 3
```
