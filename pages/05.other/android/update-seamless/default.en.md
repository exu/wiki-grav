---
title: Update Seamless
visible: false
---

[toc]

Updating devices with support for seamless updates (due to having two system partitions) is much simpler than updating devices with [only one partition](/other/android/update).

## Devices

| Device               | Notes |
| -------------------- | ----- |
| Motorola Edge 40 Pro |       |

**TODO**
