---
title: Neo Backup
visible: true
---

[toc]

## Restore

| Source           | Target               | Date       |
|------------------|----------------------|------------|
| OnePlus 3        | Xiaomi Mi 9 Lite     | 2023-03-17 |
| Xiaomi Mi 9 Lite | Motorola Edge 40 Pro | 2024-05-18 |

### Working data restore

Listed apps are working when the required data is restored.

| App                      | Backup data                | Date       | Notes                                                                                                             |
|--------------------------|----------------------------|------------|-------------------------------------------------------------------------------------------------------------------|
| AL-chan                  | APK, Data                  | 2024-05-18 | Account logged in                                                                                                 |
| AccA                     | APK, Data                  | 2024-05-18 | Profiles kept                                                                                                     |
| AdAway                   | APK, Data                  | 2024-05-18 | **Grant root permissions**                                                                                        |
| Aegis                    | APK, Data                  | 2024-05-18 | **Disable and enable biometric security**, **Create backup target directory**                                     |
| AntennaPod               | APK, Data                  | 2023-03-17 | Podcasts and progress present                                                                                     |
| Audiobookshelf           | APK, Data                  | 2024-05-18 | Account logged in                                                                                                 |
| Bitwarden                | APK, Data                  | 2024-05-18 | Account logged in                                                                                                 |
| Catima                   | APK, Data                  | 2023-03-17 | Saved cards present                                                                                               |
| Discord                  | APK, Data                  | 2024-05-18 | Account logged in                                                                                                 |
| FairEmail                | APK, Data                  | 2024-05-18 | All accounts working                                                                                              |
| FanFiction.Net           | APK, Data                  | 2023-03-17 | Account logged in                                                                                                 |
| Finamp                   | APK, Data                  | 2024-05-18 | Account logged in                                                                                                 |
| Findroid                 | APK, Data                  | 2024-05-18 | Account logged in                                                                                                 |
| Firefox                  | APK, Data                  | 2024-05-18 | Including open tabs and account                                                                                   |
| Floccus                  | APK, Data                  | 2024-05-18 | Account logged in and bookmarks present, **Create backup of browser bookmarks before. Potential for overwrites!** |
| FlorisBoard Beta         | APK, Data                  | 2024-05-18 | Settings copied without issue                                                                                     |
| FolderSync               | APK, Data                  | 2024-05-18 | Folder pairs and login kept, **Create source folders**                                                            |
| FreeOTP+                 | APK, Data                  | 2023-03-17 | TOTP Codes present                                                                                                |
| Gadgetbridge             | APK, Data                  | 2023-03-17 |                                                                                                                   |
| Gallery (Fossify)        | APK, Data                  | 2024-05-18 | Settings kept, **Transfer photos manually**                                                                       |
| ICSx5                    | APK, Data                  | 2024-05-18 | Web calendar subscriptions kept                                                                                   |
| Infinity                 | APK, Data                  | 2023-03-17 | Account logged in                                                                                                 |
| Jellyfin                 | APK, Data                  | 2024-05-18 | Account logged in                                                                                                 |
| Joplin                   | APK, Data                  | 2024-05-18 | WebDAV logged in                                                                                                  |
| Material Files           | APK, Data                  | 2024-05-18 | Settings transferred                                                                                              |
| Megalodon                | APK, Data                  | 2024-05-18 | Account logged in                                                                                                 |
| MeteoSwiss               | APK, Data                  | 2024-05-18 | Settings transferred                                                                                              |
| Metronom                 | APK, Data                  | 2024-05-18 |                                                                                                                   |
| Mihon (+ extensions)     | APK, Data                  | 2024-05-18 | Manga restored, **Restore extension APK and data before opening**                                                 |
| NewPipe                  | APK, Data                  | 2024-05-18 | Settings and playlists restored                                                                                   |
| Niagara Launcher         | APK, Data                  | 2024-05-18 | Free settings transferred, **Reverify pro subscription**, **Readd widgets**                                       |
| Organic Maps             | APK, Data                  | 2024-05-18 |                                                                                                                   |
| OsmAnd~                  | APK, Data, External data   | 2024-05-18 | Settings and maps transferred                                                                                     |
| Paseo                    | APK, Data                  | 2023-03-17 |                                                                                                                   |
| QKSMS                    | APK, Data                  | 2023-03-17 | Settings transferred, **SMS Logs are not part "data"**                                                            |
| QUIK                     | APK, Data                  | 2024-05-18 | Settings and data transferred, **Open app once, set as default and restore data again**                           |
| Shattered Pixel Dungeon  | APK, Data                  | 2024-05-18 |                                                                                                                   |
| Shizuku                  | APK, Protected device data | 2024-05-18 | Settings transferred, **Open app and run Shizuku using your preferred method**                                    |
| StreetComplete           | APK, Data                  | 2024-05-18 | Account logged in                                                                                                 |
| Subtracks                | APK, Data                  | 2023-03-17 | Account logged in, **Offline songs need to be redownloaded**                                                      |
| Symfonium                | APK, Data                  | 2024-05-18 | Accounts logged in, support ID identical                                                                          |
| TIDAL                    | APK, Data                  | 2024-05-18 | Account logged in                                                                                                 |
| Tachidesk Sorayomi       | APK, Data                  | 2024-05-18 | Settings transferred                                                                                              |
| Tachiyomi (+ extensions) | APK, Data                  | 2023-03-17 |                                                                                                                   |
| Threema Libre            | APK, Data                  | 2024-05-18 | Chats and contacts transferred                                                                                    |
| Tuner                    | APK, Data                  | 2024-05-18 |                                                                                                                   |
| Unciv                    | APK, Data, External data   | 2024-05-18 | Settings and game saves transferred                                                                               |
| VoteInfo                 | APK, Data                  | 2024-05-18 |                                                                                                                   |
| Whatsapp                 | APK, Data, Media           | 2023-03-17 | Account logged in and chat history kept, Media contains images and videos                                         |
| Wireguard                | APK, Data                  | 2024-05-18 | Includes config files, **no activated tunnels**                                                                   |
| p!n                      | APK, Data                  | 2024-05-18 | **Launch app once**                                                                                               |

### Broken data restore

Apps in this list can't be properly restored to a working state. Some offer their own backup tools while others require a new setup.

| App                         | Backup data | Date       | Notes                                                                                   |
|-----------------------------|-------------|------------|-----------------------------------------------------------------------------------------|
| Credit Suisse               | APK         | 2024-05-18 | APK only, **Put app on Magisk deny list before opening**                                |
| D4DJ                        | APK, Data   | 2024-05-18 | User ID copied, login not possible                                                      |
| Davx5                       | APK         | 2024-05-18 | APK only, No accounts linked                                                            |
| FNZsecure                   | APK         | 2024-05-18 | APK only, New setup required                                                            |
| Galaxy Buds2 Pro Manager    | APK         | 2024-05-18 | Component of Galaxy Wearable                                                            |
| Galaxy Wearable             | APK         | 2024-05-18 | New setup required                                                                      |
| Glider (Hacker News)        | APK         | 2024-05-18 | Not logged in                                                                           |
| James DSP                   | N/A         | 2024-05-18 | Version downgrade, no backup restored                                                   |
| KOReader                    | APK         | 2024-05-18 | No settings restored                                                                    |
| Librera FD                  | APK, Data   | 2023-03-17 | Settings not copied                                                                     |
| Materialistic (Hacker News) | APK, Data   | 2023-03-17 | Not logged in                                                                           |
| Medgate                     | APK         | 2024-05-15 | APK only, Crashing with restored data                                                   |
| News (Nextcloud)            | APK         | 2024-05-18 | Not logged in                                                                           |
| Nextcloud                   | APK         | 2024-05-18 | Not logged in                                                                           |
| Obtainium                   | APK, Data   | 2024-05-18 | *Built-in backup*, Settings transferred, **Add application links manually**             |
| Proton Mail                 | APK         | 2024-05-18 | APK only, Not logged in                                                                 |
| Raccoon                     | APK         | 2024-05-18 | *(untested) Built-in backup*, APK only, Crashing with restored data                     |
| Revolut                     | APK         | 2024-05-18 | APK only, Crashing with restored data                                                   |
| SBB Mobile                  | APK, Data   | 2024-05-18 | Commute restored, **Log in again**                                                      |
| SecureSign                  | APK         | 2024-05-18 | APK only, Crashing with restored data                                                   |
| Signal                      | APK         | 2024-05-18 | *Built-in backup*, APK only, Crashing with restored data, **Use official backup tools** |
| TWINT                       | APK         | 2024-05-18 | APK only, Crashing with restored data                                                   |
| ebaseSecure                 | APK, Data   | 2023-03-17 | New setup required                                                                      |

### Apps without user data

Apps in this list don't contain any data worth keeping

| App                           | Backup data | Date       |
|-------------------------------|-------------|------------|
| Arcticons                     | APK         | 2024-05-18 |
| Arcticons Material You        | APK         | 2024-05-18 |
| Codec Info                    | APK         | 2024-05-18 |
| KDE Connect                   | APK         | 2024-05-18 |
| Kontakte (Fossify)            | APK         | 2024-05-18 |
| LibreOffice Viewer            | APK         | 2024-05-18 |
| MMRL                          | APK         | 2024-05-18 |
| MuPDF viewer                  | APK         | 2024-05-18 |
| Network Survey                | APK         | 2024-05-18 |
| Simple Play Integrity Checker | APK         | 2024-05-18 |
| SmartPack-Kernel Manager      | APK         | 2024-05-18 |
| Treble Info                   | APK         | 2024-05-18 |
| VLC                           | APK         | 2024-05-18 |
| WiFiAnalyzer                  | APK         | 2024-05-18 |
| mpv                           | APK         | 2024-05-18 |
