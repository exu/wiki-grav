---
title: Update
visible: true
---

[toc]

Because I root my phone, I have to follow some specific steps to update my system correctly.

**Requires a PC**

## Devices

| Device           | Notes |
| ---------------- | ----- |
| Xiaomi Mi 9 Lite |       |

## Acquire LineageOS Update Zip-file

The newest update can either be downloaded through the built-in updater, exported to local storage and copied to the PC, or downloaded directly from [download.lineageos.org](https://download.lineageos.org)

## Patch Boot image with Magisk

1. Download latest Boot image
2. Patch with Magisk
3. Copy to PC

## Update LineageOS

1. Enable USB Debugging through ADB
2. Run `adb reboot sideload`
3. Update using `adb sideload LINEAGE_UPDATE.zip`

## Reinstall Magisk

_An alternative is installing [Lygisk](https://github.com/programminghoch10/Lygisk), which allows for automatic reinstallation after OTA Upgrades_

1. Boot to fastboot: `adb reboot bootloader`
2. Run `fastboot flash boot MAGISK_PATCHED.img`
3. Reboot to system `fastboot reboot`

And the update's done!

## Links

> [Update LineageOS on pyxis](https://wiki.lineageos.org/devices/pyxis/update#sideloading-from-recovery)  
> [Magisk Installation > Patching Images](https://topjohnwu.github.io/Magisk/install.html#patching-images)
