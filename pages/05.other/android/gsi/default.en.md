---
title: GSI
visible: true
---

[toc]

## Devices

This is a list of my own devices using a GSI ROM.

| Device           | Notes                                        |
| ---------------- | -------------------------------------------- |
| Xiaoxin Pad 2022 | Only system_b works, system_a is not present |

## Installation

1. Open a terminal and type `adb reboot bootloader`
2. After bootloader is up. Type `fastboot devices` to check if your device is found properly.
3. Type `fastboot flashing unlock` **WARNING: User data will be deleted with this step**
4. Type `fastboot reboot fastboot` to launch into standalone fastboot mode
5. Navigate to the folder where `vbmeta.img` and `boot.img` are saved
   `fastboot flash --disable-verity --disable-verification vbmeta vbmeta.img`
   `fastboot flash --disable-verity --disable-verification boot boot.img`
6. Get the currently active system slot `fastboot getvar current-slot`
   - Change the slot using `fastboot --set-active=a/b`
7. Type `fastboot delete-logical-partition product_a` then `fastboot delete-logical-partition product_b`
8. Last step install GSI with `fastboot flash system [image].img`

> [XDA Thread on GSI with Lenovo Tab M10 Plus 3rd Gen](https://xdaforums.com/t/installing-custom-rom-gsi-magisk-on-lenovo-tab-m10-plus-3rd-gen-tb-125fu.4480023/)

## Updating

1. Open a terminal and execute `adb reboot bootloader`
2. `fastboot reboot fastboot`
3. `fastboot getvar current-slot`
4. Change slot to the inactive one: `fastboot --set-active=a/b`
5. `fastboot flash system [image].img`
6. `fastboot reboot`
7. Open Magisk and direct install
8. Reboot

## Wipe everything

1. Open a terminal and execute `adb reboot bootloader`
2. `fastboot reboot fastboot`

`fastboot erase system_a`  
`fastboot erase system_b`  
`fastboot erase userdata`
