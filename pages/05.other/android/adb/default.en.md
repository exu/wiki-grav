---
title: ADB
visible: true
---

[toc]

## Full Storage backup
The easiest, and probably intended way to copy files using adb can be seen in the following command.  
`$ adb pull -a -p /sdcard/ ./<target folder>`  
The example here makes a copy of all content in "sdcard", the user's data directory.  
This is probably still the best way, with compression afterwards on the host system. Simply, because other options don't allow browsing of the file structure properly.  
Additionally, this will show a progress meter, allowing for guesses when the transfer will be complete.  

Compress the directory using this command.  
`$ tar -cv -I"zstd -19 -T0" -f sdcard.tar.zst <folder>`  

### Compressed backup
> [Stackoverflow answer](https://stackoverflow.com/a/39429196)  

Using this command, a backup can be pulled as well. This time however, it is passed to zstd for compression.  
Tar on the phone doesn't do much except go through all files and send them to the terminal's STDOUT. All the compression happens on the host.  
*All data has to be transfered first. very slow with USB 2.0*  
*Also not readable after the transfer*  
`$ adb exec-out "tar -cf - /sdcard/* 2>/dev/null" | zstd -19 -T16 -v -o sdcard-2022-11-18.zst`  

*testing*  
`$ adb exec-out 'GZIP="-9" /system/xbin/busybox tar -czf - /sdcard/*' > sdcard.tar.gz`  
