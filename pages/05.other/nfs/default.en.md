---
title: NFS
visible: true
---

[toc]

Last modified: 2024-01-26

## Linux Server

`# apt install nfs-server`

Shares can be configured in `/etc/exports`  
`<mountpoint>   <allowed_ip>(<option1>,<option2>) <allowed_ip2>(<option1>,...)`

### Options

```
ro: specifies that the directory may only be mounted as read only
rw: grants both read and write permissions on the directory
no_root_squash: is an extremely dangerous option that allows remote “root” users the same privilege as the “root” user of the host machine
subtree_check: specifies that, in the case of a directory is exported instead of an entire filesystem, the host should verify the location of files and directories on the host filesystem
no_subtree_check: specifies that the host should not check the location of the files being accessed withing the host filesystem
sync: this just ensures that the host keeps any changes uploaded to the shared directory in sync
async: ignores synchronization checks in favor of increased speed
```

_Example single host:_  
`/mnt/nfs   192.168.1.123(rw,sync,no_subtree_check)`

_Example whole subnet:_  
`/mnt/nfs   192.168.1.0/24(rw,sync,no_subtree_check)`

Apply new config by restarting the service.  
`# systemctl restart nfs-server`

Show configured shares  
`$ cat /var/lib/nfs/etab`

### UID mapping

> [Mapping UID and GID of local user to the mounted NFS share](https://serverfault.com/a/632315)

## Linux Client

`# pacman -S nfs-utils`  
`# apt install nfs-common`

Mount through terminal  
`# mount -t nfs4 (ip):(mountpoint) (local mountpoint)`

Can also be mounted with fstab

## Windows Client

Search for `Turn Windows features on or off`

Check everything under `Services for NFS` and click "OK"

Mount as mapped network drive  
`mount -o anon \\(ip)\(mountpoint) (letter):`
