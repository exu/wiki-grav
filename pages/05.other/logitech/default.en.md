---
title: Logitech
visible: true
---

[toc]

## Logitech Unifying Receiver

Logitech receivers can be managed using a few different programs on Linux.  
One of the available options is [Solaar](https://github.com/pwr-Solaar/Solaar)

To use Solaar, create the group `plugdev` and add your user to it.  
See [Users and Groups](/linux/users-and-groups)

### Tested hardware

| Peripheral            | Receiver      | Solaar version |
| --------------------- | ------------- | -------------- |
| Logitech MX Keys Mini | Logitech Bolt | 1.1.8          |

> [Archwiki: Logitech Unifying Receiver](https://wiki.archlinux.org/title/Logitech_Unifying_Receiver)
