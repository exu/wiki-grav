---
title: FFMPEG
visible: true
---

[toc]

## List supported codecs and formats

`$ ffmpeg -codecs`

`$ ffmpeg -formats`

## Video Encoding

### H.264

> [H.264 Encoding Guide](https://trac.ffmpeg.org/wiki/Encode/H.264)

### AV1

> [AV1 Encoding Guide](https://trac.ffmpeg.org/wiki/Encode/AV1)

#### libaom

```sh
ffmpeg -i "/mnt/storage/MediaLibrary/input/Joker/test.mkv" -metadata title="Joker" -disposition 0 \
    -c:v libaom-av1 -crf 23 -b:v 0 -cpu-used 6 -row-mt 1 -map 0:v:0 -metadata:s:v:0 title="Video" \
    -c:a libopus -b:a 768k -ac:a 8 -map 0:a:0 -map 0:a:3 -metadata:s:a:0 title="English [7.1ch]" -metadata:s:a:0 language=eng -metadata:s:a:1 title="German [7.1ch]" -metadata:s:a:1 language=ger -disposition:a:0 default \
    -c:s copy -map 0:s:0 -map 0:s:1 -metadata:s:s:0 title="English [PGS]" -metadata:s:s:0 language=eng -metadata:s:s:1 title="German [PGS]" -metadata:s:s:1 language=ger -disposition:s:0 default \
    /mnt/storage/MediaLibrary/output/Joker/test-libaom-av1.mkv
```

Additional settings for increased speed and cpu usage:

```
-g 239: keyframes every ~10s (fps * 10)
-tiles 2x2: multiple parallel encoding tiles to speed up performance (4 in total here)
```

```sh
ffmpeg -i "/mnt/storage/MediaLibrary/input/Joker/test.mkv" -metadata title="Joker" -disposition 0 \
    -c:v libaom-av1 -crf 23 -b:v 0 -cpu-used 6 -row-mt 1 -g 239 -tiles 2x2 -map 0:v:0 -metadata:s:v:0 title="Video" \
    -c:a libopus -b:a 768k -ac:a 8 -map 0:a:0 -map 0:a:3 -metadata:s:a:0 title="English [7.1ch]" -metadata:s:a:0 language=eng -metadata:s:a:1 title="German [7.1ch]" -metadata:s:a:1 language=ger -disposition:a:0 default \
    -c:s copy -map 0:s:0 -map 0:s:1 -metadata:s:s:0 title="English [PGS]" -metadata:s:s:0 language=eng -metadata:s:s:1 title="German [PGS]" -metadata:s:s:1 language=ger -disposition:s:0 default \
    /mnt/storage/MediaLibrary/output/Joker/test-libaom-av1-tiling-keyframes.mkv
```

#### SVT-AV1

```sh
ffmpeg -i "/mnt/storage/MediaLibrary/input/Joker/test.mkv" -metadata title="Joker" -disposition 0 \
    -c:v libsvtav1 -crf 23 -preset 8 -g 239 -map 0:v:0 -metadata:s:v:0 title="Video" \
    -c:a libopus -b:a 768k -ac:a 8 -map 0:a:0 -map 0:a:3 -metadata:s:a:0 title="English [7.1ch]" -metadata:s:a:0 language=eng -metadata:s:a:1 title="German [7.1ch]" -metadata:s:a:1 language=ger -disposition:a:0 default \
    -c:s copy -map 0:s:0 -map 0:s:1 -metadata:s:s:0 title="English [PGS]" -metadata:s:s:0 language=eng -metadata:s:s:1 title="German [PGS]" -metadata:s:s:1 language=ger -disposition:s:0 default \
    /mnt/storage/MediaLibrary/output/Joker/test-libsvtav1-keyframes.mkv
```

## Audio Encoding

> [High Quality Audio Encoding Guide](https://trac.ffmpeg.org/wiki/Encode/HighQualityAudio)

## Video Quality

### VMAF

> [A practical guide for VMAF](https://medium.com/a-practical-guide-for-vmaf-481b4d420d9c)

_Note: The order of the input videos is important. Make sure to place the distorted video first_

```sh
ffmpeg -i (distorted) -i (original) -filter_complex libvmaf -f null -
```
