---
title: VNC
visible: true
media_order: vnc-linux-pic1-example.png
---

[toc]

## Linux Server

For the VNC Server we will be using tightVNC.  
`# apt install tightvncserver`

Initial setup and starting VNC server  
`vncserver`

You will have to enter a password  
Optionally, a view-only password can be created as well.

Kill VNC server  
`vncserver -kill :1`

Edit the `xstartup` file in `.vnc` to your liking.

_Example with xfce_

```sh
#!/bin/sh

xrdb $HOME/.Xresources
unset SESSION_MANAGER
unset DBUS_SESSION_BUS_ADRESS
exec startxfce4
```

### Change password

`vncpasswd`

You can also add a view-only password

## Windows Server

Install tightVNC to get a VNC Client and also a VNC Server in one package for windows.  
The server will be started automatically.

One important setting is `Require VNC authentication`, which allows you to define a password for viewing and interacting with the remote pc.

## Linux Client

Install Remmina with libvncserver to get client functionality.  
`# pacman -S remmina libvncserver`  
![Picture showing the usage of VNC with Remmina](vnc-linux-pic1-example.png)

## Windows Client

Install tightVNC to get a VNC Client and also a VNC Server in one package for windows.
