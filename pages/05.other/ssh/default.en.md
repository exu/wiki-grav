---
title: SSH
visible: true
---

[toc]

## Linux Server

### Installation

#### Debian

`# apt install openssh-server`

#### Arch

`# pacman -S openssh`

`# systemctl enable ssh`

### Configuration file

`/etc/ssh/sshd_config`

Make sure to restart the sshd service after changes.

### Change port

Uncomment `Port` and set any port number

### Root login

`PermitRootLogin` setting

```
yes -> Able to log in with password as root
```

### Password Authentication

`PasswordAuthentication` setting

```
yes -> Allow login with passwords
no -> Only allow ssh keys
```

On OpenBSD also set `KbdInteractiveAuthentication` to `no`

## Windows Server

Open PowerShell as administrator  
`Add-WindowsCapability -Online -Name OpenSSH.Server`

Start service  
`Start-Service sshd`

Enable service  
`Set-Service -Name sshd -StartupType 'Automatic'`

Check whether firewall rule exists  
`Get-NetFirewallRule -Name *ssh*`

Create firewall rule for port 22  
`New-NetFirewallRule -Name sshd -DisplayName 'OpenSSH Server (sshd)' -Enabled True -Direction Inbound -Protocol TCP -Action Allow -LocalPort 22`

## Linux Client

### Configuration file

`/etc/ssh/ssh_config`

### Connect to non-standard port

`$ ssh -p (port) (user)@(ip)`

### X11 passthrough

`$ ssh -X (user)@(ip)`

### ssh keys

Create new key:  
`$ ssh-keygen`

_Example_ for ed25519 key:  
`$ ssh-keygen -t ed25519`

The "-C" flag can be used to add comments in ssh key files.

Enable the ssh key:  
`$ ssh-copy-id -i (public key file) (user)@(ip/domain)`

If you are copying the ssh key from a different client, use the "-f" flag  
`$ ssh-copy-id -f -i (public key file) (user)@(ip/domain)`

## Windows Client

Open PowerShell as administrator  
`Add-WindowsCapability -Online -Name OpenSSH.Client`

## SSH Tunnel systemd Service

SSH tunnels can be created as systemd services

_Example tunnel:_  
`ssh -NTfL 8080:webserver:80 user@remotehost`

### Tunnel settings

Save the file under `/etc/systemd/system/(application/tunnel name)`

```
PATH_TO_KEY=(ssh key path)
LOCAL_PORT=8080
REMOTE_ADDR=webserver
REMOTE_PORT=80
REMOTE_USER=user
REMOTE_HOST=remotehost
```

### Tunnel service

This service can be used with multiple different "tunnel settings" files. Similar to how the wg-quick service works with different wireguard configs.  
Save this file under `/etc/systemd/system/local-tunnel@.service`

```
[Unit]
Description=Setup a local tunnel to %I
After=network.target

[Service]
EnvironmentFile=/etc/default/local-tunnel@%i
ExecStart=/usr/bin/ssh -i ${PATH_TO_KEY} -o ServerAliveInterval=60 -o ExitOnForwardFailure=yes -nNT -L ${LOCAL_PORT}:${REMOTE_ADDR}:${REMOTE_PORT} ${REMOTE_USER}@${REMOTE_HOST}
RestartSec=15
Restart=always
KillMode=mixed

[Install]
WantedBy=multi-user.target
```

Finally, the tunnel can be enabled  
`# systemctl daemon-reload`  
`# systemctl enable --now local-tunnel@(application/tunnel name)`

## CLI Options

### No matching host key type found

Full error message:  
`Unable to negotiate with <host> port <port>: no matching host key type found. Their offer: <comma separated list of host keys>`

This happens, when a host key is used that has been deprecated in the locally installed ssh client.  
Use the option `-oHostKeyAlgorithms=+<host key type>` with ssh to connect regardless.

## References

- [Windows OpenSSH Installation](https://docs.microsoft.com/en-us/windows-server/administration/openssh/openssh_install_firstuse)
- [SSH Tunnel as systemd service](https://ivanmorenoj.medium.com/ssh-tunnel-as-systemd-service-3c53bd157ac1)
