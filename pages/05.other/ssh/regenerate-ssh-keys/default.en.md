---
title: "Regenerate SSH Keys"
visible: true
---

[toc]

## Remove from known_hosts

`$ ssh-keygen -R (server name)`

## Debian

Remove the old Hostkeys  
`# rm -v /etc/ssh/ssh_host_*`

Generate new Hostkeys  
`# dpkg-reconfigure openssh-server`
