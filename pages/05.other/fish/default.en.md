---
title: Fish
visible: true
---

[toc]

## For loop

### Iterating over number sequence

`for i in (seq 1 10); echo $i; end`  
Output:

```
1
2
3
4
5
6
7
8
9
10
```

If you want all numbers to be padded to equal lengths use the `-w` flag with `seq`  
`for i in (seq -w 1 10); echo $i; end`  
Output:

```
01
02
03
04
05
06
07
08
09
10
```
