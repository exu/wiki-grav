---
title: 'eBook DRM Removal'
visible: true
---

[toc]

## Providers
This is a non-exhaustive list of which sellers use which DRM format  

Source | DRM free | Kindle | Adobe DRM 
---    | ---      | ---    | ---
Amazon | -        | x      | -
Kobo   | ?        | -      | x

## Kindle
> [Wiki how to remove DRM](https://github.com/apprenticeharper/DeDRM_tools/wiki/Exactly-how-to-remove-DRM)  

> [DeDRM and KFX](https://github.com/apprenticeharper/DeDRM_tools/discussions/1581)  

> [Python Setup for DeDRM](https://github.com/apprenticeharper/DeDRM_tools/issues/1456)  

## Adobe DRM
> [Kobo help article about Adobe DRM](https://help.kobo.com/hc/en-us/articles/360017814074-Add-eBooks-with-Adobe-Digital-Editions-)  

> [Calibre plugin to download Adobe DRM locked ebooks](https://github.com/Leseratte10/acsm-calibre-plugin)  
