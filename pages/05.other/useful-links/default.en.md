---
title: 'Useful Links'
visible: true
---

[toc]

## Internet Speed Tests

Cloudflare provide a more detailed than normal speed test website.

![Cloudflare internet speed test page](speedtest-cloudflare.png)

> [Internet Speed Test by Cloudflare](https://speed.cloudflare.com/)

## git - ours & theirs

> [git - ours & theirs by Nitay Megides](https://nitaym.github.io/ourstheirs/)
