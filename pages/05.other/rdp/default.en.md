---
title: RDP
visible: true
---

[toc]

## Linux Server

`# apt install xrdp`

`# systemctl enable xrdp`

Put the desktop environment you want to start in `.xsession`

_Example_  
`xfce4-session`

`# systemctl restart xrdp`

### Change port

Edit `/etc/xrdp/xrdp.ini`

Change the value of `port` to what you want

`# systemctl restart xrdp`

## Windows Server

### Windows Server Edition

Go to `Local Server` in the Server manager.  
There should be an option called `Remote Desktop`. Click on it and allow remote connections.  
If you refresh the view now, `Remote Desktop` should show as enabled.

#### Allow unlimited RDP sessions

Enter `gpedit` in the search bar

Go to `Administrative Templates>Windows Components>Remote Desktop Services>Remote Desktop Session Host>Connections`

Disable `Limit number of connections`

Disable `Restrict Remote Desktop Services users to a single Remote Desktop Services session`

Reboot the Server

### Windows Pro Edition

Go to `Remotedesktop` in the settings under `System`

#### Change port

_PowerShell as admin_

Check port in use currently:  
`Get-ItemProperty -Path 'HKLM:\SYSTEM\CurrentControlSet\Control\Terminal Server\WinStations\RDP-Tcp' -name "PortNumber"`

Change port:  
`Set-ItemProperty -Path 'HKLM:\SYSTEM\CurrentControlSet\Control\Terminal Server\WinStations\RDP-Tcp' -name "PortNumber" -Value (port)`

Firewall exception:  
`New-NetFirewallRule -DisplayName 'RDPPORTLatest' -Profile 'Public' -Direction Inbound -Action Allow -Protocol TCP -LocalPort (port)`

Reboot the PC

## Linux Client

### Installation

Use Remmina as client and install freerdp to get support for RDP.

`# pacman -S remmina freerdp`

### Configuration

Example configuration:  
![rdp-linux-client-pic1-example.png](/rdp-linux-client-pic1-example.png)

#### Set different port

![rdp-linux-client-pic2-port.png](/rdp-linux-client-pic2-port.png)

## Windows Client

Enter `Remote Desktop Connection` in Windows search.  
The target computer can be specified by IP or name  
After clicking on `connect` the user will be asked to insert the username and password.

### Use different port

![rdp-winpro-client-pic1-example-port.png](/rdp-winpro-client-pic1-example-port.png)

## References

- [ArchWiki Remmina](https://wiki.archlinux.org/index.php/Remmina)
- [Azure RDP configuration](https://docs.microsoft.com/en-us/azure/virtual-machines/linux/use-remote-desktop)
- [ArchWiki xrdp](https://wiki.archlinux.org/index.php/Xrdp)
