---
title: Vim
visible: true
---

[toc]

## Get output from command

`:r!(command)`

_Example to get UUID for a disk_  
`:r!blkid /dev/(partition) -sUUID -ovalue`

## Write as sudo user

`:w !sudo tee %`

## Replace strings

Globally replace strings  
`:%s/foo/bar/g`

Replace strings in line 6 to 10  
`:6,10s/foo/bar/g`
