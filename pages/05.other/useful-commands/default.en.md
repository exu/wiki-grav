---
title: "Useful Commands"
visible: true
---

[toc]

## Imagemagick

### Splitting PDF files

```sh
convert -density 600 {INPUT.PDF} -crop 50x100% +repage {OUT.PDF}
```

```
-density: adjusts the quality of the resulting pdf. Higher values look better, but take longer to process
-crop 50x100%: this splits the pdf into "left" and "right". 100x50% would split into "top" and "bottom"
```

## Ghostscript

### Merge multiple PDF files

Ghostscript is preinstalled on a lot of Linux systems and can quite easily be used to merge and optimize multiple PDF files into one.

[shuser]

```sh
gs -q -sDEVICE=pdfwrite -dPDFSETTINGS=/prepress -o merged.pdf <first>.pdf <second>.pdf
```

[/shuser]

> [Merge / convert multiple PDF files into one PDF](https://stackoverflow.com/a/19358402)

## Find

### Change filtered permissions

Using find with its `exec` switch one can set different permissions based on the usual find filters.  
One example would be only changing file or directory permissions.

```sh
find (directory) -type f -exec chmod 744 {} +
```

Replacing `-type f` with `-type d` would execute the `chmod` for directories instead.

## Arch Linux

### Detect AUR package rebuilds

Install the package `extra/rebuild-detector`  
It will create a `pacman` hook to check which packages need to be rebuild, but can also be executed with the following command.

[shuser]

```sh
checkrebuild
```

[/shuser]

The packages might be rebuilt automatically a lot of the time, but sometimes it is necessary to for a rebuild using the AUR helper.  
This is an example using `paru`

[shuser]

```sh
paru -S --rebuild=yes <package>
```

[/shuser]

## Various

### Unlock user after too many failed sudo attempts

Use `faillock` to check for failed sudo attempts

[shroot]

```
faillock --user <user>
```

[/shroot]

Unlock a locked account  
[shroot]

```
faillock --user <user> --reset
```

[/shroot]

> [Unlock user after too many failed sudo attempts by Josh Sherman](https://joshtronic.com/2021/05/23/unlock-user-after-too-many-failed-sudo-attempts/)

### Overwrite disk with pseudorandom data

Using openssl on CPUs with AES acceleration one can create pseudorandom data with high speeds.  
Much faster than `/dev/urandom` at least

```sh
openssl enc -aes-128-ctr -md sha512 -pbkdf2 -nosalt -pass file:/dev/urandom < /dev/zero | pv > {TARGET DISK}
```

Around 2GiB/s on my Ryzen 7 1700x if output to `/dev/null`

### Formatted csv in terminal

> [From Pretty CSV viewing on the Command Line](https://www.stefaanlippens.net/pretty-csv.html)

```sh
column -t -s, < {FILE.CSV}
```

### Download directory from webdav

Using `wget`, it's possible to download directories recursively from WebDAV.

```sh
wget -r -nH -np --cut-dirs=1 --user={USERNAME} --password={PASSWORD} https://WEBDAVHOST/DIR/DIR
```

### Find USB speed in use

First, the USB device you want to check has to be selected

[shuser]

```sh
lsusb
(out)Bus 004 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub
(out)Bus 003 Device 003: ID 0c45:652f Microdia Backlit Gaming Keyboard
(out)Bus 003 Device 002: ID 046d:c084 Logitech, Inc. G203 Gaming Mouse
(out)Bus 003 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
(out)Bus 002 Device 002: ID 0781:55a3 SanDisk Corp.  SanDisk 3.2Gen1
(out)Bus 002 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub
(out)Bus 001 Device 002: ID 2972:0047 FiiO Electronics Technology FiiO BTR5
(out)Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
```

[/shuser]

Now, using the ID, the USB class used can be determined

[shuser]

```sh
lsusb -d 0781:55a3 -v | grep bcdUSB
(out)bcdUSB               3.20
```

[/shuser]

### Change file modify time

Using `touch` it is possible to change the timestamps on a file.  
_Note: The file has to be owned by the user executing the command_

Example changing the access and modify timestamps:

[shuser]

```
touch -a -m -t 202307291506.07 <file>
```

[/shuser]

```
-a: accessed time
-m: modified time
-t: timestamp - [[CC]YY]MMDDhhmm[.ss] time format
```

Alternatively to `-t` it is also possible to use `-d` for a looser format.  
[ISO8061](https://en.wikipedia.org/wiki/ISO_8601) obviously works as well.

[shuser]

```
touch -a -m -d "2 hours ago" <file>
touch -a -m -d "2023-07-29T00:23" <file>
```

[/shuser]

> [Linux - modify file modify/access/change time](https://stackoverflow.com/questions/40630695/linux-modify-file-modify-access-change-time)  
> [How can I change the date modified/created of a file?](https://askubuntu.com/questions/62492/how-can-i-change-the-date-modified-created-of-a-file)

### Create a random temporary directory / file

Using `mktemp`, a randomly named file or directory will be created in `/tmp`

Create a file:  
[shuser]

```sh
mktemp
```

[/shuser]

Create a directory:  
[shuser]

```sh
mktemp -d
```

[/shuser]

Save the output into an environment variable for future referencing

sh / Bash:  
[shuser]

```sh
export TMPDIR=$(mktemp -d)
```

[/shuser]

Fish:  
[shuser]

```fish
set TMPDIR (mktemp -d)
```

[/shuser]
