---
title: Git
visible: true
---

[toc]

## Reset everything to selected branch

Useful for getting to the same state as upstream

```sh
git reset --hard (upstream)/(branch)
git pull (upstream) (branch)
```

Finally force push all of this into your own repo

## Get Pull Request from foreign repo

_Example with neofetch_

Add remote if you haven't already done that  
`git remote add dylanaraps https://github.com/dylanaraps/neofetch.git`

Remotes can be show with `git remote show`

Fetch desired commits  
`git fetch dylanaraps a0221c51ff4c8ce834d7e3431f2770b6879de009`

Cherry pick commits  
`git cherry-pick -m 1 a0221c51ff4c8ce834d7e3431f2770b6879de009`

Resolve whatever conflicts arise
