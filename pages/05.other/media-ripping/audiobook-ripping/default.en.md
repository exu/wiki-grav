---
title: Audiobook Ripping
visible: true
---

[toc]

## Audible

1. Get your Audible activation data
2. Convert the downloaded `.aax` file

### Audible activation data
Multiple possible ways exist to extract activation data from Audible `.aax`-files  

The simples way is just using [this AAX checksum resolver](https://audible-tools.kamsker.at/)  
Alternatively [audible-activator](https://github.com/inAudible-NG/audible-activator) can be used as well  

### Convert AAX file
Using `ffmpeg` and the activation bytes, a file can easily be convertet to `m4b`  

[shuser]
```sh
ffmpeg -activation_bytes [XXXXXXXX] -i [Audiobook].aax -c copy [Audiobook].m4b
```
[/shuser]
