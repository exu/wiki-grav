---
title: Music Ripping
visible: true
---

[toc]

## Getting accurate rips
Getting accurate rips from CDs is a challenge. Although the data on the CD is digital, the reading process is analog and can contain errors.  
Different drives have different levels of accuracy. [Here is a list of various drives and how they compare](https://forum.dbpoweramp.com/showthread.php?48320-CD-Drive-Accuracy-2022)  

To get the most accurate rip possible, multiple reads are often necessary and comparisons with other people who ripped the same CD also helps.  

## Whipper
> [Project Repo](https://github.com/whipper-team/whipper)

