---
title: SSHFS
visible: true
---

[toc]

## Linux Client

`# apt install sshfs`

`# pacman -S sshfs`

Mount remote filesystem  
`sshfs (user)@(ip/domain):(remotepath) (mountpoint)`

_Example with Windows host:_  
`sshfs admin@192.168.1.123:/ /mnt/tmp`

## Windows Client

Install [WinFSP](https://github.com/billziss-gh/winfsp)  
Install [sshfs-win](https://github.com/billziss-gh/sshfs-win)

### Usage

_No path = start in remote user's home directory_

#### GUI

Map a new network drive in Windows Explorer  
`\\sshfs\(user)@(ip/domain)\(path)`

#### Terminal

Mount drive  
`net use (letter): \\sshfs\(user)@(ip/domain)\(path)`

Show mounted drives  
`net use`

Remove mounted drive  
`net use (letter): /delete`

## References

- [sshfs](https://github.com/libfuse/sshfs)
- [sshfs-win](https://github.com/billziss-gh/sshfs-win)
