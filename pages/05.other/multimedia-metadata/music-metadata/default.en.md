---
title: "Music Metadata"
visible: true
---

[toc]

My personal guidelines for music metadata

## Links

> [Music Metadata Style Guide](https://musicbiz.org/wp-content/uploads/2016/04/MusicMetadataStyleGuide-MusicBiz-FINAL2.0.pdf)  
> [Kid3 Frame List](https://docs.kde.org/trunk5/de/kid3/kid3/commands.html#frame-list)  
> [MP3 Tag Field Mappings](https://docs.mp3tag.de/mapping/)  
> [ID3 v2.4.0 Standard](https://id3.org/id3v2.4.0-frames)  
> [Common Metadata Tags](https://borewit.github.io/music-metadata/doc/common_metadata.html)

## Notes

Using separate fields for artists only works with `Vorbis` metadata and will be converted to `artist1;artist2` automatically by ffmpeg  
Multiple `artist` fields are not recognized in ID3v2, use `;` to separate instead
