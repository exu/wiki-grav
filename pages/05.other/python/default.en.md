---
title: Python
visible: true
---

[toc]

## Packaging

### Building

If `setup.py` or `pyproject.toml` are properly configured, the command below can be used to build a python package.

```sh
python -m build
```

### Install local package

Pip can be used to install a local package for testing purposes.

```sh
pip install dist/{PACKAGE NAME}.whl
```

### Push to PyPI

```sh
python -m twine upload --skip-existing dist/*
```

To test packaging stuff, it is recommended to push packages to testpypi instead.

```sh
python -m twine upload --skip-existing --repository testpypi dist/*
```

## Exit on Keyboard Interrupt

```python
try:
    <put your code here>
except KeyboardInterrupt:
    <stuff that needs to be done before exiting>
    # return SIGINT code
    exit(130)
```

A potentially better way of handling keyboard interrupts is using a signal handler.

```python
import time
import signal
import sys

def signal_handler(signal, frame):
    print("\nKeyboard Interrupt detected.")
    # add your cleanup code here
    sys.exit(0)

# register signal handler
signal.signal(signal.SIGINT, signal_handler)

while True:
    time.sleep(1)
```
