---
title: 'Endpoint Windows Firewall Policy'
visible: true
---

[toc]

Profile type: Templates > Endpoint Protection  
Configuration settings: Windows Firewall

## View Firewall rules

Firewall rules distributed using Intune currently do not appear in the `wf.msc` GUI or with the PowerShell `Get-NetFirewallRule` module.  
The only place to view these rules is the registry.
```
HKEY_LOCAL_MACHINE\SYSTEM\ControlSet001\Services\SharedAccess\Parameters\FirewallPolicy\Mdm\FirewallRules
```
