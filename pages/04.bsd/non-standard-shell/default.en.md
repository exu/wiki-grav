---
title: "Non-Standard Shell"
visible: true
---

[toc]

When trying to use a non-standard shell, `chsh` will throw the following error:  
`chsh: /usr/local/bin/zsh: non-standard shell`

To fix this, add the shell's path you want to use to `/etc/shells`
