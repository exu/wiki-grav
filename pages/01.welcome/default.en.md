---
title: Welcome
visible: true
---

[toc]

Welcome to my personal wiki.

This is a collection of knowledge, instructions and tutorials accumulated over the years.

Any information was accurate to my knowledge when it was written. This might not be the case anymore, or maybe I was wrong from the start.  
Many pages are written assuming my own knowledge and experience. Maybe they won't help anybody else.
