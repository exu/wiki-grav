# Style guide

## Code blocks

PrismJS is used to provide syntax highlighting

### Bash / SH

Shell highlighting uses additional keywords in markdown to set a desired prefix

Available options are `[shroot]` and `[shuser]`

Usage:

````
[shroot]

```
echo hello
```

[/shroot]
````

To filter outputlines, prepend them with `(out)`.  
Don't put a space between the keyword and the output

```
lsusb -d 0781:55a3 -v | grep bcdUSB
(out)bcdUSB               3.20
```

#### Sudo

Although I try to avoid using `sudo`, in some cases it is quite handy. Examples can be seen on the Nextcloud page, where commands **need** to be executed as `www-data`.  
Following a [post on lemmy](https://programming.dev/post/569497), I learned of a cool way to do this with just `su`

This example copies a file in the web directory, using the user `www-data`. Command blocks using this style should be prefixed with `#` using `[shroot]`

````
[shroot]

```
su -s /bin/sh -c 'cp /var/www/html/html1 /var/www/html/html2' www-data
```

[/shroot]
````

## Variables / Fill your own

Values that should be replaced by the reader are generally represented this way:  
`[VALUE]`
