<?php
namespace Grav\Plugin\Shortcodes;

use Thunder\Shortcode\Shortcode\ShortcodeInterface;

class Prismjsshroot extends Shortcode
{
    public function init()
    {
        $this->shortcode->getRawHandlers()->add('shroot', function(ShortcodeInterface $sc) {
            $content = preg_replace('/^```.*(\n)?$/m', '', $sc->getContent());
            $content = trim($content);
            return '<pre class="command-line language-sh" data-prompt="#" data-continuation-str="\" data-filter-output="(out)" tabindex="0"><code class="language-sh">'.$content.'</code></pre>';
        });
    }
}
?>
