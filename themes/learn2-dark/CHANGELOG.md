# v0.2.1
## 30.06.2024
### Removed
- Google fonts

# v0.2.0
## 19.11.2022
### Changed
- Renamed theme to learn2-dark
- Reformat changelog how I like it

### Removed
- Removed read-icon and "clear history" toggle

# v0.1.1
## 19.11.2022
### Added
- Added LangSwitcher to header sidebar

# v0.1.0
##  19.11.2022
### Changed
- Transfer css from custom.css into scss files
