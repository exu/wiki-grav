# Learn2 Dark Theme

The **Learn2 Dark** Theme is for [Grav CMS](http://github.com/getgrav/grav).  This README.md file should be modified to describe the features, installation, configuration, and general usage of this theme.

## Description

A custom dark theme based on Learn2

## SCSS Watching
To compile scss into css, install the package `ruby-sass`  

```sh
scss --watch scss:css-compiled
```

For one-time executions:  

```sh
scss --update scss:css-compiled
```
